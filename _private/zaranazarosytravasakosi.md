---
layout: post_private
title: Scythe
---


Tu uploaduj:

<datalist id="mena">
    <option value="Palo"></option>
    <option value="Juro"></option>
    <option value="Tobiáš"></option>
    <option value="Samo"></option>
    <option value="Matúš"></option>
    <option value="Mišo"></option>
</datalist>
<datalist id="mat">
    <option value="1"></option>
    <option value="2"></option>
    <option value="2A"></option>
    <option value="3"></option>
    <option value="3A"></option>
    <option value="4"></option>
    <option value="5"></option>
</datalist>

<button id="authorize_button" onclick="handleAuthClick()">Authorize</button>
<button id="signout_button" onclick="handleSignoutClick()">Sign Out</button>

<form>
    <input type="date" id="date" onclick="checkDate()">
    <table>
        <tr>
            <td>Frakcia</td>
            <td>Meno</td>
            <td>Mat</td>
            <td>Hviezdy</td>
            <td>Hexy</td>
            <td>Suroviny</td>
            <td>Bonus</td>
            <td>Mince</td>
            <td>Spolu</td>
            <td>Poradie</td>
        </tr>
        <tr style="background-color:white;">
            <td>Polania</td>
            <td><input id="pol_meno" list="mena" onchange="detectName('pol')"></td>
            <td><input id="pol_mat" list="mat" hidden="true"></td>
            <td><input id="pol_hviez" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('pol')"></td>
            <td><input id="pol_hex" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('pol')"></td>
            <td><input id="pol_sur" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('pol')"></td>
            <td><input id="pol_bonus" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('pol')"></td>
            <td><input id="pol_minc" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('pol')"></td>
            <td><div id="pol_sum"></div></td>
            <td><input id="pol_por" placeholder="--" type="number" hidden=true min="1" max="7"></td>
        </tr>
        <tr style="background-color:green;">
            <td style="color: white;">Albion</td>
            <td><input id="alb_meno" list="mena" onchange="detectName('alb')"></td>
            <td><input id="alb_mat" list="mat" hidden="true"></td>
            <td><input id="alb_hviez" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('alb')"></td>
            <td><input id="alb_hex" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('alb')"></td>
            <td><input id="alb_sur" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('alb')"></td>
            <td><input id="alb_bonus" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('alb')"></td>
            <td><input id="alb_minc" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('alb')"></td>
            <td><div id="alb_sum" style="color: white;"></div></td>
            <td><input id="alb_por" placeholder="--" type="number" hidden=true min="1" max="7"></td>
        </tr>
        <tr style="background-color:blue;">
            <td style="color: white;">Nordic</td>
            <td><input id="nor_meno" list="mena" onchange="detectName('nor')"></td>
            <td><input id="nor_mat" list="mat" hidden="true"></td>
            <td><input id="nor_hviez" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('nor')"></td>
            <td><input id="nor_hex" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('nor')"></td>
            <td><input id="nor_sur" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('nor')"></td>
            <td><input id="nor_bonus" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('nor')"></td>
            <td><input id="nor_minc" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('nor')"></td>
            <td><div id="nor_sum" style="color: white;"></div></td>
            <td><input id="nor_por" placeholder="--" type="number" hidden=true min="1" max="7"></td>
        </tr>
        <tr style="background-color:red;">
            <td style="color: white;">Rusviet</td>
            <td><input id="rus_meno" list="mena" onchange="detectName('rus')"></td>
            <td><input id="rus_mat" list="mat" hidden="true"></td>
            <td><input id="rus_hviez" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('rus')"></td>
            <td><input id="rus_hex" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('rus')"></td>
            <td><input id="rus_sur" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('rus')"></td>
            <td><input id="rus_bonus" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('rus')"></td>
            <td><input id="rus_minc" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('rus')"></td>
            <td><div id="rus_sum" style="color: white;"></div></td>
            <td><input id="rus_por" placeholder="--" type="number" hidden=true min="1" max="7"></td>
        </tr>
        <tr style="background-color:purple;">
            <td style="color: white;">Togawa</td>
            <td><input id="tog_meno" list="mena" onchange="detectName('tog')"></td>
            <td><input id="tog_mat" list="mat" hidden="true"></td>
            <td><input id="tog_hviez" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('tog')"></td>
            <td><input id="tog_hex" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('tog')"></td>
            <td><input id="tog_sur" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('tog')"></td>
            <td><input id="tog_bonus" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('tog')"></td>
            <td><input id="tog_minc" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('tog')"></td>
            <td><div id="tog_sum" style="color: white;"></div></td>
            <td><input id="tog_por" placeholder="--" type="number" hidden=true min="1" max="7"></td>
        </tr>
        <tr style="background-color:yellow;">
            <td>Crimea</td>
            <td><input id="cri_meno" list="mena" onchange="detectName('cri')"></td>
            <td><input id="cri_mat" list="mat" hidden="true"></td>
            <td><input id="cri_hviez" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('cri')"></td>
            <td><input id="cri_hex" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('cri')"></td>
            <td><input id="cri_sur" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('cri')"></td>
            <td><input id="cri_bonus" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('cri')"></td>
            <td><input id="cri_minc" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('cri')"></td>
            <td><div id="cri_sum"></div></td>
            <td><input id="cri_por" placeholder="--" type="number" hidden=true min="1" max="7"></td>
        </tr>
        <tr style="background-color:black;">
            <td style="color: white;">Saxony</td>
            <td><input id="sax_meno" list="mena" onchange="detectName('sax')"></td>
            <td><input id="sax_mat" list="mat" hidden="true"></td>
            <td><input id="sax_hviez" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('sax')"></td>
            <td><input id="sax_hex" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('sax')"></td>
            <td><input id="sax_sur" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('sax')"></td>
            <td><input id="sax_bonus" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('sax')"></td>
            <td><input id="sax_minc" hidden="true" placeholder="--" type="number" min="0" onchange="updateFrakcia('sax')"></td>
            <td><div id="sax_sum" style="color: white;"></div></td>
            <td><input id="sax_por" placeholder="--" type="number" hidden=true min="1" max="7"></td>
        </tr>
    </table>
    <input type="checkbox" id="included_in" name="included_in" value="" checked=checked>
        <label for="included_in">Má sa zahrnúť do všeobecných štatistík?</label><br>
    <input type="checkbox" id="mats" name="mats" value="" checked=checked>
        <label for="mats">Nahadzuješ aj maty?</label><br>
    <textarea placeholder="Poznámka" id="note" style="width: 30%;"></textarea>
    <br>
    <button id="uploadButton" onclick="upload()" type='button'>Upload</button>
    <button id="resetButton" onclick="resetTable()" type='button'>Reset table</button>
    <div id="error" style="color: red;"></div>
</form>

<script src="scythe/scythe.js" type="text/javascript"></script>
<link rel="stylesheet" href="scythe/scythe.css">
<script async defer src="https://apis.google.com/js/api.js" onload="gapiLoaded()"></script>
<script async defer src="https://accounts.google.com/gsi/client" onload="gisLoaded()"></script>