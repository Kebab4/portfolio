---
layout: post_private
title: Ak zvažuješ, že by si ma niečím obdaroval/a
redirect_from: /2023/11/20/cgfxza92s2ubd24bp4fbgvj6hxqvq6v8ge46_darceky.html
---

<!--
link:

http://oxford.kebis.sk/private/cgfxza92s2ubd24bp4fbgvj6hxqvq6v8ge46_darceky.html
http://localhost:4000/private/cgfxza92s2ubd24bp4fbgvj6hxqvq6v8ge46_darceky.html
-->

- V prvom rade, nerob to iba *pro forma*. Keď už, aj drobnosť viem oceniť. A nemusí to byť zrovna dar. Pekné slovo, idenie na hlbinu (nenarážam na hru), spoločný čas, či prekvapenie ma vedia veľmi potešiť.
- Osobné veci a s príbehom sú skvelé. Napr. spomienkové...
- Dobré dary sú tiež také, ktoré mi pomôžu budovať kamarátstvá a príbuzenské vzťahy. Napr. lístky na koncert, hra, niečo cestovateľské... 
- Úžitkové veci sú fajné, ale to naozaj nie je ľahké trafiť. Raz mi treba kventinku do izby, raz by som potreboval rukavice, raz nový kábel do nabíjačky, raz voňavku... Pri tomto by som bol dva krát opatrný.
- Dobrou knihou nič nepokazíš. Ale čo považujem za dobrú knihu?
    - Mám rád knihy, ktoré otvárajú horizonty (napr. filozofia, teológia, mystika, dobre napísaná história či geopolitika) a sú overené časom.
    - Slovenská poézia je fajn, ale nekupuj iba na základe internetových rebríčkov.
    - Populárno-vedecké z princípu odmietnuť nemôžem :D
    - Beletria môže byť, ale iba ak máš fakt tušenie, že sa mi to bude páčiť.
    - Opatrne s kupovaním kníh! Nestíham ich čítať tak rýchlo, ako ich dostávam.
    - Viem poskytnúť zoznam kníh, po ktorých poškulujem.
- Turistika – Nuž, potrebujem toho veľa, ale veľmi by som sa bál, ak by mi niekto iný kupoval vybavenie. Ale existuje nemalá množina vecí, ktoré človek potrebuje a nevadí, keď mu ich kúpi dakdo iný. Čelovku už mám.
- Oblečenie – Keďže sa idem oblečenia zbavovať, neviem či stojí za to, aby mi teraz druhí dávali ďalšie.

<!--

<br>

# Zoznam vecí, ktoré si chcem/potrebujem kúpiť

UPOZORNENIE! Mnohé z týchto vecí si chcem vybrať sám

- Turistická bunda (!!! sám si vybrať)
- Merino ponožky a slipy (a tričko?)
- Turisticko-športová mikina (!!! sám si vybrať)
- Turistický ruksak (!!! sám si vybrať)
- Turistická fľaša (? chcem?)
- Turistický nožík (!!! sám si vybrať)
- Pepper sprej na medveďov
- Zvonček na medveďov
- Stan/Hamak pre jedného/dvoch (!!! sám si vybrať)
- Externá nabíjačka

- Zimné topánky (!!! sám si vybrať)
- Ruksak/Aktovka na život (!!! sám si vybrať)
- Ľadvinka (!!! sám si vybrať)
- Hodinky (!!! sám si vybrať)
- Rukavice (? kožené? športové?) (! musia byť teplé)
- Šiltovka (!!! sám si vybrať)
- Voňavka (! pozor, čo vonia jednému nemusí voňať druhému)

- Old style foťák na filmy (? chcem?) (!!! sám si vybrať)
- Slúchatká (? stratil som, nenájdu sa?) (! mikrofón; cestovateľské; aby neboleli uši)
- Externý monitor (? treba?)


- Voňavé hovadiny do izby
- Obraz do izby (!!! sám si vybrať)

- Akordeón (? chcem?) (!!! sám si vybrať)
- Fujara (? chcem?) (!!! sám si vybrať)
- Nová gitara (? chcem?) (!!!!!! sám si vybrať)

<br>

# Príklady kníh, po ktorých poškulujem

Humanitné
- Classics si radšej požičiam :D
- Komparatívna religionistika (budhizmu, islam, hinduizmus...)
- Niečo dobré od kardinála Špidlíka (viem si požičať: Vedy - umeny - náboženství; Prameny svetla)
- George Pell nenapísal niečo dobré?
- Simon Weil (viem si požičať: Tiaž a milosť; Duchovná autobiografia)
- Iris Murdoch (čo takto The Sovereignty of Good ?)
- Karl Rahner (viem si požičať: Mária, Matka Pána; O svátostech v církví)
- Thomas Merton
- O púštnych otcoch
- O cirkevných otcoch – nie pre odborníkov
- ...

<br>
Próza
- Niečo od Chestertona (napr. Večný človek?)
- "Meno ruže" máme, však?
- Aký je Paolo Gordiano?
- Nejaká dobrá slovenská moderná (+-) próza by bodla. Aký je Mitana?
- ...

<br>
Poézia
- Slovenská poézia 20. storočia – prijmem prakticky akúkoľvek :D

<br>
Náučné
- Niečo, čo by mi pekne a pomaly vysvetlilo geológiu a vývoj vzniku hôr
- Etymologický slovník slovenského jazyka
- Slovník slovenských nárečí
- Slovenská história trošku s nadhľadom – nie, nemám rád zoznamy kráľov a roky vládnutí. Mám rád, niečo čo ponúka princíp.
- Ľudské telo pre človeka, ktorý zabudol všetku biológiu
- Kryptomeny a blokchain (! toto si radšej nechám poradiť od spolužiakov)
- Niečo, čo mi konečne, laicky (aj s kúskom matematiky), vysvetlí ekonomiku
- Nejakú fajnú knihu o Cirkvi v prvých storočiach – ideálne o liturgii
- Predrománska architektúra
- Študijna Biblia (? zvažujem, či je férové to chcieť, keďže v tomto období až tak po Biblii nesiaham)
- ...
- Around the world in 80 games
- Esher, Bach, Gauss


-->
