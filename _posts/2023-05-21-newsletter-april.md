---
layout: post
title: Newsletter III.
subtitle: 
banner: "/assets/images/newsletters/III/uvodna.jpeg"

---
*Dvaja slováci MFoCSu a legendárna tabuľa MFoCS room so svojou counting society v ľavom rohu*

**Hoci sa Hillary term skončil pred viac ako mesiacom, keďže po ňom nasledovalo skúškové obdobie, tak až teraz, z autobusu na Tatry hľadiac, dostal som sa k písaniu tohto novinko-listu. Druhá časť môjho príbehu v Oxforde je podobná všetkým druhým častiam – rýchlejšia, monotónnejšia, hlbšia, zaneprádznenejšia, ale aj plná novôt. O čom bol Hillary term?**

## Rozširovanie obzorov

Veľmi rýchlo som pochopil, že v Oxforde sa nenaučím najviac, keď budem robiť iba to, čo sa odo mňa očakáva, ale že zvedavosť, odvaha, ale aj schopnosť povedať „toto je nad moje sily“ sú cestou k poznaniu.

Tento trimester som strávil veľa času v našej MFoCS room (MFoCS je názov môjho programu), ktorá bola svedkom rôznych „aha“ momentov. Keď mi skončila prednáška, prišiel som tam a vedel som, že tam nájdem ostatných, ktorých sa budem môcť pýtať na časti, ktorým som nerozumel, alebo mi pomôžu s problémami na cviko, na ktorých som sa zasekol. V piatky podvečer sme si zorganizovali tzv. „Tins & Talks“ (tins od plechoviek🍺), kde niekto z nás prezentuje ostatným o nejakej téme, o ktorej vie viac, alebo ktorej sa venoval. 

Krásnymi sa však stali aj mnohé ne-akademické stránky MFoCSu. Napr. v ľavom hornom rohu našej tabule sme si začali počítať a už sme sa dopočítali niekde nad 3 tisíc (pozor! človek nemôže pripočítať dva krát za sebou). Nechýbajú aj filmové večery v prednáškových sálach, debaty pri obedoch a večer v puboch, či len (ne)obyčajné hranie Ultimate Tic Tac Toe.

Po skúsenosti z minulého trimestra som sa rozhodol vyskúšať radšej viac kurzov a potom niektoré ťažšie, alebo nudnejšie nechať tak. Týmto spôsobom som si prešiel 6 kurzami a úprimne, nemyslím si, že som ich flákal (priemerne si človek zoberie 3). Medzi nimi boli kurzy o logike, kvantových počítačoch, kurz o teórii hier, či o teórii typov. Miesto jednolivého popisu som sa tento-krát rozhodol radšej vybrať jednu aplikáciu týchto poznatkov a viac sa mi snáď podarí napísať do samostatných blogov. (Áno, ja viem, že zatiaľ som tie blogy moc nepísal, ale v poslednej dobe toho bolo fakt dosť.)

# O KVANTOVOM POČÍTAČÍ V PIATICH VETÁCH

Kvantový počítač je počítač, ktorého základná jednotka nie je bit (0 alebo 1), ale kvantový bit (qubit), ktorý, na rozdiel od svojej klasickej verzie, je akákoľvek komplexná kombinácia 0 a 1 (pre jednoduchosť napr. 66% 0 + 34% 1). Už tu vidno, že kvantový počítač vie jedným qubitom reprezentovať nespočetne viac informácie ako klasický bit. Ďalšou vecou však je, že qubity sa vedia spolu „skamarátiť“ (entangle) a ich „kamarátstvo“ je zrazu nosič ďalšej informácie, inými slovami, skamarátené qubity sú viac ako dva samostatné qubity. Toto je ďalšia úžasná vec, ktorá nám zrazu umožňuje efektívne vyriešiť niektoré problémy, ktorých vyriešenie trvá na klasických počítačoch kvadratický či exponenciálny čas. O tom, ako  viac tento počítač funguje, aký vplyv má na infomačnú bezpečnosť, či aké aplikácie má napr. v chémií snáď napíšem niečo aj do blogu; zatiaľ vás odkážem na tento rozhovor [(https://tinyurl.com/skqub)](https://tinyurl.com/skqub). 


## Nie iba matika

Niekto raz povedal, že „život je to, čo sa deje popri tom čo robíme“. A nejak podobne to vyzeralo aj v prípade iných ako matematickch akcií a prednášok. Môj mail je každý deň doslova zasypaný pozvánkami na rôzne udalosti, z ktorých iba zlomok mám šancu vvyskúšať. Z tých zopár čo mi utkveli v pamäti spomeniem: koncert študentského jazzového orchestra, talk o architektúre katedrály Sagrada Familia, formálna debata o eutanázií,  debata o transubstanciácii, večera s Danielom Bonevacom (dajte si do youtube a pochopíte), prednáška o Enigme, zimné preteky vo veslovaní (Torpids).

Bolo však aj niekoľko seminárov, či skôr prednášok, ktoré som sa rozhodol navštevovať pravidelne. Konkrétne prednášková séria emeritného profesora Petra Hackera o filozofii Ludwiga Wittegensteina a pondelkové Aquinas Group kde sme rozoberali otázky o Bohu vo svetle Sumy Teologickej od Tomáša Akvinského. Obidve tieto série boli pre mňa úžasné prínosné a pri oboch som pochopil, akí veľkí akademici sa okolo mňa pohybujú.


<p class="image-in-blog">
<img style="max-width:200px;" src="/assets/images/newsletters/III/enigma.jpg" alt="Enigma naživo"/>
<em>Reálna Enigma s fungujúcou mechanikou</em>
</p>

## O veslovaní
Už je mi to tu trošku trápne písať, lebo nechcem aby to vyznelo, že polku času iba veslovujem 😅. Dva týždne pred veslárskymi pretekmi to však naozaj platilo. Skorý raný budík (niekedy aj pred 6:00), bicyklovanie k lodenici, vyberanie lode, vonku niekedy aj 6°C a vlhko, a potom hodina fučania, kde ste niekedy práve vy ten, kto to kazí. Odmena však bola sladká vo forme účasti na zimých pretekov Torpids. O tom však v nejakom dlhšom príspevku. S veslovaním nateraz končím. Nie že by ma to nebavilo, práveže som si veľmi obľúbil tento šport a aj tie ranné tréningy boli fajn. Avšak berie to čas a chcem sa teraz venovať iným veciam. Aspoň som si vyskúšal tento klasický „Oxford experience“.

<p class="image-in-blog">
<img style="max-width:300px;" src="/assets/images/newsletters/III/hmla.jpg" alt="hmla na rieke"/>
<em> Pri takomto mlieku sme to radšej na vodu kvôli bezpečnosti nehrotili </em>
</p>


**Návyky z predošlého trimestera** – chodiť na prednášky z iných oblastí 🫳, vytvoriť debatnú skupinku 👎, behať do parku 🫳, skôr vstávať 👍 a chodiť viac pešo 👍.

**Návyky na ďalší trimester** – obdivovať parky a načúvať vtáčikom, každý týždeň navštíviť inú society, zapojiť sa do rugby alebo behať, chodiť na prednášky z iných oblastí.

## Čo teraz?

Teraz ma čaká tretí trimester, no už nebudem mať žiadne kurzy (iba ak jeden dobrovoľný). Namiesto toho začínam písať diplomovú prácu a tú budem písať až do konca augusta. V septembri ma potom čakajú obhajoby a po nich už len promócie. Diplomovú prácu bude viesť prof. Worrell s ktorým budem robiť na téme z prieniku teórie čísel (prvočísla, delitele, zvyšky po delení…) a teórie vypočítateľnosti (Turingov stroj, konečno-stavový automat…). Cieľom je zovšeobecniť jednu vetu, ktorá hovorí, aké skupiny čísel vieme vyjadriť pomoc určitých výpočtových modelov a nájsť pre túto vetu dôkaz. Držte mi palce! Bude to sranda 😉

## Exkurz: Colleges

Oxfordská univerzita začalo ako veľmi decentralizovaná a decentralizovanou aj zostala. Už v 13. storočí sa v meste začali združovať študenti s profesormi, ktorí postupne hľadali možnosť spoločného a od-nájmu-nezávislého bývania. Toto sa postupne pretransformovalo do tzv. kolégií, kde študenti bývali priamo s profesormi. Postupne pribúdali rôzne regulácie, právomoci a štandardizácie, až sa z konceptu kolégií stala nutnosť; teda každý študent Univerzity musí byť člen nejakej college a každý študent na college je vlastne študentom Univerzity. Kolégiá si stavali pre svojich študentov ubytovania, jedálne, kaplnky, knižnice, a zároveň mali na starosti aj vedenie mladších študentov – mladší študenti majú časť výučby na college a nie na fakulte. College je teda inštitúcia starajúca sa najmä o welfare študentov: pomáha so štúdiom či poskytuje zázemie a domov. Ja som mal na svojej college (Kellogg College) hneď v prvý týždeň množstvo akcií kde sme spoznali ostatných z college a pravidelne sa organizujú akcie pre študentov. Zároveň bývam na campuse mojej college, často tam chodím jesť a občas využívam aj knižnicu. Mám tiež prístup k športoviskám a ľuďom, ktorí mi vedia pomôcť s administratívou, mentoringom, psychickým zdravím, kontaktom na lekára, či opraviť bicykel.

Medzi college-ami je veľká rivalita ale aj hrdosť. Každá college má svoj erb, históriu, legendy a niekedy aj divné zvyky. St. John’s College je najbohatšia, na Oriel je veľa študentov teológie, v Keble majú otrasné jedlo, Wolfson majú skvelý veslársky klub, Christ Church je najviac „posh“, medzi Lincoln a Brasenose je brána, ktorú slávnostne otvárajú iba jeden deň v roku…

<p class="image-in-blog">
<img style="max-width:500px;" src="/assets/images/newsletters/III/erby.jpg" alt="erby college-ov"/>
<em>Erby college-ov a v centre erb univerzity</em>
</p>




19.4.2023