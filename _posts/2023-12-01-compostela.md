---
layout: post
title: Camino de Compostela
subtitle: O vstupovaní do prítomnosti aleb púť je ako život
banner: "/assets/images/blog/compostela/sipky.jpg"

---

## Deň 1. Most medzi Hendaye a Irún

Stojím na najsevernejšom moste medzi Francúzskom a Španielskom. Je neskoré ráno a len pred chvíľou som vystúpil z vlaku vo francúzskom mestečku Hendaye. Ohliadam sa za seba a vidím tých 300 metrov, ktoré som doposiaľ prešiel. Pozerám sa pred seba a snažím sa predstaviť si tých 800 kilometrov, ktoré ešte prejsť potrebujem. V hlave si rátam, koľko by mi to trvalo ísť diaľnicou a koľko lietadlom. Autom tam je človek za deň. Lietadlom to nie sú ani 2 hodiny. Mne to má trvať niečo viac, ako 30 dní. Efektívne, však?

Vyberám si "stratiť" mesiac svojho života kráčaním za cieľom, ktorý ma sám osebe až tak neťahá, no tí pred mnou povedali, že cesta doň stojí za to. Stojí za čo? Áno, očakávam, že niečo na tejto ceste nájdem. Že nájdem odpovede na svoje otázky. A možno ešte hodnotnejšie: že nájdem svoje otázky. Otázky, ktoré mi dajú nadhľad, múdrosť a hlavne prinesú pokoj mojej duši. Posledné tri roky by sa dali charakterizovať ako plné inšpirácií, premien, očarení, sebaspoznávania, prerámcovávania... Myslím, že ich potrebujem trochu prežuť. A tuším, že práve kráčanie je jedna z dobrých foriem prežúvania.

Beriem do ruky svoju tlačidlovú Nokiu a sám pred sebou sa rozhodujem, že si za celú cestu urobím iba dve fotky: jednu selfie tu a jednu pred katedrálou v cieli. (Áno, môj tlačidlový telefón má aj foťák a dokonca hlasovo riadený selfie mód 😎) Chytrý telefón som nechal doma. Bolo to prekvapujúco ľahšie, než som si myslel. Cítim sa, ako by som sa vrátil 30 rokov v čase, kedy neexistovali sociálne siete a kedy bola papierovaná mapa to jediné, na čo ste sa mohli spoľahnúť. Je to úžasne oslobodzujúce nemôcť si skontrolovať správy, nemôcť zahnať nudu prečítaním si novín či nemôcť fotiť každý kopec a zdieľať každý moment. Paradox, nie?



<p class="image-in-blog">
<img style="max-width:300px;" src="/assets/images/blog/compostela/pred.jpg" alt="fotka pred"/>
<em>Pred...</em>
</p>

A teraz čo? ... Nuž, tak skúsim ísť rovno. Snáď nájdem cestu. Nestihol som si kúpiť sprievodcu s podrobnou mapou a tá jediná, ktorú som si vytlačil mi hovorí iba toľko, že sa musím nejako dostať na hrebeň kopcov napravo. Neviem, kde dnes budem spať. Neviem, kde a čo budem jesť. Neviem, ako si zoženiem svoj kredenciál (doklad o tom, že som pútnik). Neviem, ako nájdem cestu a ani ako sú značené. Neviem ani po španielsky. Viem, že *cesta* sa povie *camino* a *kam* sa povie *dondé*. Myslím, že to môže byť celkom dobrodružné. Neistôt sa nebojím. Veď je to, koniec koncov, iba kráčanie.


<p class="image-in-blog">
<img style="max-width:600px;" src="/assets/images/blog/compostela/pasai.jpg" alt="Albergue v Pasai Donibane"/>
<em>Tu som napokon v prvý deň spal.</em>
</p>

## Deň 5. Gernika-Lumo

Keď sa povie slovo púť, človek si väčšinou predstaví kráčanie osamote a dlhé premýšľanie. A teraz tu sedím s Holanďankou, Nemcom a Mexičanom, rozprávajúc sa o všetkom a chrániac sa pred ostrým obedným slnkom v tieni budov jedného z najznámejších baskytských mestečiek. Cítim sa ako vo filme, keď sa náhodnými udalosťami stane z jednotlivca spoločenstvo. Ak sa toto stalo za 5 dní, čo sa stane za 30? Budem s nimi kráčať až do konca? Čo sa s nami stane? Staneme sa priateľmi? Ak sa odlúčime, uvidíme sa ešte niekedy?

Naozaj sa mi teraz nechce z tejto partie odchádzať. Som sklamaný z prvých dní, ktoré nepriniesli mojej mysli nič, iba ticho. Videl som síce nádherné útesy, rozhľahlé more, stráne s ovcami, zabudnuté dediny, dlhočízne pláže, či zakúsil lokálne tradície. Ale moje vnútro akosi nie je spokojné. Snažil som sa systematicky premýšľať, ale keď ste sami, vaše myšlienky jednoducho utečú. Moje denníky sa plnia iba keď zastanem a iba keď sa sílim niečo písať.

Pri týchto mojim spoločníkoch cítim, že niečo sa môže udiať. Možno práve oni prinesú zvuk do môjho ticha. Nechávam sa viesť. V mysli sa mi ozývajú slová jedného mnícha, s ktorým som sa rozprával pred odchodom: "Cesta ťa povedie!" Tak veru, a veď aj títo moji spoločníci sú súčasťou cesty.


<p class="image-in-blog">
<img style="max-width:600px;" src="/assets/images/blog/compostela/parta.jpg" alt="Spolupútnici"/>
<em>Tých zopár spolupútnikov, s ktorými som počas prvej polky svojej púte aj kráčal.</em>
</p>


## Deň 14. Niekde za San Vicente de la Barquera

Dnes som vyrazil ešte pred východom Slnka a lesnými cestičkami ma cesta priviedla až na toto odpočívadlo v Bohom zabudnutom údolí. Je síce pre autá, ale veľa áut tu nechodí. Uprostred malého parčíka stromov sa skrývajú lavičky a hojdačka vyrobená z pneumatiky. Listy stromov sa už sfarbujú jesenne a príjemné svetlo Slnka skrz ne preniká až na mňa ako sa húpem na hojdačke. Keby som bol dieťa, tento moment by sa mi isto vryl do pamäte veľmi hlboko. Toto miesto dýcha pokojom a akýmsi čarom. Nechce sa mi odtiaľto odísť. Nechce sa mi s nikým pretekať.

Pred ôsmimi dňami, keď som vchádzal do Bilbaa som sa lúčil s ostatnými z partie, dúfajúc, že sa naše cesty ešte znova pretnú. Odvtedy som stretol iba Marie. Moje dni boli omnoho viac osamelé, krajina viac betónová a moja myseľ skormútená. Až jeden Brit – dôchodca, ktorý už ide svoje piate camino – mi povedal, nech sa neviažem na onú partiu, ale nech kráčam ďalej svojím tempom. Prídu ďalší pútnici a ďalší ľudia, ktorých stretnem. Jeho slová boli pre mňa uistením a akosi odštartovali novú etapu kráčania. 



<p class="image-in-blog">
<img style="max-width:600px;" src="/assets/images/blog/compostela/la_arena.jpg" alt="la arena"/>
<em>Typické skalnaté, plážové a zazelenané severné pobrežie Španielska (a vlny)</em>
</p>

Ako som prekročil hranicu Kantábrie, prežil som niekoľko úžasných dní. Najprv mesto Laredo so svojou nekonečnou plážou a ubytovanie v kláštore Trinitariánok. Potom putovanie a rozhovor s češkou Adelou a prespanie v Albergue (hostel pre pútnikov) Güemes, ktorý vedie jeden staručký cestovateľ a dobrovoľníci mu chodia pomáhať s hosťami. Napokon však Santillana de Mar, v ktorej som prežil jedny z najkrajších momentov zatiaľ. To mesto, to ubytovanie s tou záhradou a tou kaplnkou. Ten obraz, ktorý som tam videl. No a potom tie veľhory, ktoré tieto dni môžem vidieť na juhu a tí oslíkovania, ktorí mi lemujú cestu.

Už som zo seba (skoro) zhodil jarmo očakávaní, že na niečo musím prísť. Učím sa spočinúť v momente a nechcieť viac. Na ústnej harmonike už viem zahrať aj nejakú tú pesničku. Toto je život! Ráno sa zobudiť a vedieť, že jediné, čo stačí, je kráčať.


<p class="image-in-blog">
<img style="max-width:300px;" src="/assets/images/blog/compostela/vitraz.jpg" alt="Vitráž"/>
<em>Vitráž v modlitebni ubytovania v Santiallana de Mar.</em>
</p>

## Deň 24. Borres

Počasie sa zmenilo z letného na prudko jesenné – veterné, chladné a hlavne upršané. Je 5 hodín ráno a neviem spať. Víchrica a nehorázne silný dážď udierajú do útleho domčeka slúžiaceho pre pútnikov, v ktorom odpočívam spolu s desiatimi ďalšími, ktorí majú všetci v pláne vyraziť do tohto otrasného počasia akonáhle vyjde Slnko. Opäť ma rozbolel priehlavok na ľavej nohe a v mojom vnútri stojím na rázcestí medzi ustráchanosťou a bláznovstvom.

Pred asi týždňom sa mi zapálil ľavý priehlavok (nart) a musel som neplánovane zostať v meste Lugo 2 dni navyše, kým sa mi noha nezlepší. V tom čase som prežíval veľký smútok, lebo som si myslel, že do Compostely kvôli zraneniu nedôjdem. Našťastie sa mi prilepšilo, a tak som sa vyzbrojený masťami a paličkami vydal ďalej. Teraz sa mi však bolesť vrátila a priviedla pochybnosti o tom či som schopný v tomto vetre a daždi a so zranenou nohou prejsť najťažšiu etapu celej púte, ktorá ide celá mimo civilizáciu. Zdá sa mi však zbabelé čakať na mieste celý deň a dúfať, že sa niečo zlepší alebo sa vydať na obchádzku, ktorá by mi cestu predĺžila o deň.


<p class="image-in-blog">
<img style="max-width:600px;" src="/assets/images/blog/compostela/terapia2.jpg" alt="V Lugu"/>
<em>Kúsok prežívania z Luga.</em>
</p>

Tieto dni sú veľmi bojovné. Noha nie je to jediné, čo mi púta pozornosť, ale aj intenzívny dážď, ktorý preniká do všetkých častí môjho ruksaku a mojich topánok. Zo zúfalstva som si v obchode kúpil čierne 50 litrové sáčky do košov a prevliekol nimi môj ruksak a seba. Okrem toho som si kúpil sandále a skúšal v nich kráčať, no dážď a nové sandále nebol až tak dobrý nápad.

Príroda je teraz iná – divočina. V závoji oblakov a spŕškov dažďa je človek vďačný za každý výhľad. Hoci aj za hmlu vystupujúcu zo vzdialených dolín. V mysli sa vraciam pár dní dozadu, kedy som si sadol k vodopádu a iba sedel a nechával sa ohlušovať jeho tichom. Mysľou mi chodí pieseň z filmu Brat Slnko a sestra Luna: *"if you want to live life free, take your time, go slowly..."*. Sloboda. Kto si?



<p class="image-in-blog">
<img style="max-width:600px;" src="/assets/images/blog/compostela/penaseita.jpg" alt="Peňaseita"/>
<em>V chatrči uprostred hôr divajúc sa na "slíže" dažďa.</em>
</p>

## Deň 32. Tridsať kilometrov pred cieľom

V mysli sa obzerám späť. Prešiel som skoro 800 kilometrov. Prakticky celý sever Španielska. Je to neuveriteľné. Predo mnou leží už len 30, ktorých si rozdelím na dva dni, nech si ten posledný deň užijem. Tieto dni reflektujem moju cestu. Prešla rýchlo a zdá sa mi ako sen. Tým, že som nemal prístup na internet, a teda som nebol v priamom kontakte s nikým z domova mám pocit, akoby som žil akýsi dlhý sen. Pravda, to, že som nemal mobil, malo celkom veľký vplyv na to, ako som prežíval čas, vnímal prostredie okolo a aké dobrodružstvá sa mi prihodili. Už len napríklad to, že miesto fotenia som si musel veci kresliť. Zrazu človek vníma omnoho viac detailov, keď musí veci prevádzať na papier.

Počas cesty som stretol mnohých pútnikov. Niektorých som iba videl, s inými som prešiel kus cesty. Stretol som Čecha, ktorý vyrazil od svojho domu a kráča už päť mesiacov. Stretol som mladého Mexičana, s ktorým som sa niekoľko dní rozprával o láske, spravodlivosti, slobodnej vôli či utrpení. Stretol som mladého doktora z Brazílie, ktorý si nedávno prešiel rozvodom a rozpadom svojho tímu. Stretol som staršieho pána, ktorý si v ruksaku niesol svoj život a nevedel, kam pôjde, keď dôjde do Compostely. Tí, čo už idú túto púť niekoľkí raz hovoria, že púť je ako život.


<div style="text-align: left; font-style: italic; margin-left: 10%;">
Púť je ako život.<br>
Púť má svoj začiatok a svoj koniec.<br>
Púť je plná plánov, ktoré sa zrúcajú veľmi rýchlo.<br>
Púť je o tom kráčať. To stačí.<br>
Púť je veľká lekcia pokory.<br>
Púť je hľadanie slobody.<br>
Púť je dostať to, čo potrebujete a nie to, čo chcete.<br>
Púť je ako sen, ktorý si pamätáte aj keď sa zobudíte.<br>
Púť má svoj začiatok a svoj koniec.<br>
Púť sa začína, keď prídete do Compostely.<br>
Púť je ako život.<br>
Život je púť.
</div>
<br>



<p class="image-in-blog">
<img style="max-width:300px;" src="/assets/images/blog/compostela/po.jpg" alt="fotka po"/>
<em> ... a po.</em>
</p>

<div style="font-size: small">PS: Tento blog je skonštruovaný spätne, no verne.</div>

<!--


príbeh z mostu medzi Handaye a Irun * začaitok, očakávania, čo nemám, čo neviem, kde ma vyhodili, čo idem hľadať

príbeh z Pasai Donibane

príbeh z Deba

príbeh z Guerniky * myšlienky nemám, mám kamošov, baskytsko, počasie, príroda, pláže, iba kráčaj, španielčina

príbeh z La Arena *

príbeh z Guemes

príbeh zo Santiallana de Mar

príbeh z San Vicent de la Barquera

príbeh z odpočívadla * opustil som partiu, ale bola to lekcia, po zopár ťažkých dňoch, kráčal som s Marie a s Adelou, už niekoľko dní som v absolútne úžasných ubytkách, také a také, tieto dni sú plné momentov iba bytia, toto odpočívadlo je ako z rozprávky, koleso, pokoj, strom, jeseň, lavička...

príbeh z Pendueles

príbeh z Pinares de Pria

príbeh z Escamplero 

príbeh z Borres * fúka vietor, prišlo ťažšie počasie, bol som zranený, myslel som si, že to už nedokončím, hory sú blízko, výhľady účasné, je tu zima, slabé ubytko, včera to bolo úžasné, navarili nám tu fajnové večeru, stretávam tú švédov, staršiu brazílčanku, španiela, čile a ukrajinka...

príbeh z Grandas de Salime

príbeh z rána za Lugom

príbeh z mestečka pred Lavacolla * Švédi a mobil, očakávanie konca, dážďa a otrasné počasie, Antonio a stretnutie v Compostele, Ricardo a debaty o všetkom, astúria

príbeh z lietadla cestou domov



Niekedy človek potrebuje pauzu. Niekedy chce zažiť dobrodružstvo. Niekedy chce viac spoznať samého seba. A niekedy si na to aj nájde čas.

Púť do Santiaga de Compostela priťahuje dnes mnohých a pre mnohé dôvody. Väčšina ľudí nie sú veriaci, no veľa z nich chce okúsiť čaro tejto cesty – čaro kráčania a kráčania a kráčania a...

Akonáhle som obhájil svoju diplomovú prácu, vedel som, že chcem vyraziť a mal som trochu viac ako mesiac, kým budem mať promócie. Vybral som si severnú cestu, ktorá začína na hranici Francúzska a Španielska (Irún). Vďaka bratovi, ktorí už na púti bol, som vedel, ako sa minimalisticky zbaliť. Ale aj tak sa nikdy nezbalíte dokonale. Veľa blogov som si nečítal a ani som si nepozeral fotky. Veď keď prídem, zistím a uvidím. Aspoň budem mať prekvapenia a dobrodružstvo.

Nemal som so sebou smartfón (chytrý telefón). Napadlo mi to veľmi spontánne a nápad sa mi páčil. Bolo to skvelé rozhodnutie. 


-->