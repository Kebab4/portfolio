---
layout: post
title: Newsletter I.
subtitle: 
banner: "/assets/images/newsletters/I/radcam.jpeg"

---
*Po matriculation pred Radcliffe Camera v sub fusc. Napravo Brasenose college a naľavo All souls college*


**Po prílete, dvoch prípravných týždňoch a po dvoch týdňoch jesenného trimestra posielam krátky prehľad o tom, aká bola moja cesta do Oxfordu, aké sú moje prvé dojmy a aj krátky exkurz – slovník obsahujúci oxfordsko špecifické výrazy.**

## Prvé dojmy a prekvapenia

Ak ste už niekedy do Oxfordu zavítali, tak vás asi neprekvapí keď poviem, že samotné mesto je **úchvatné** a architektonicky bohaté. Mesto netrpelo vojnami a tak sa tu zachovalo veľa historických budov až z 11. storočia. Aj preto sa chodím veľmi rád prechádzať do centra, či do „mojej“ štvrte, kde sú úžasné, viac ako 150 staré, viktoriánske domčeky.

Samotné mesto mi príde vcelku malé a to vytvára pocit domova, **komornej atmosféry** a vo výsledku aj cítiť, že na týchto uliciach žijú mladí ľudia. Je pravda, že cez víkendy sa tu privalia turisti, ale to vôbec neuberá z čara tohto miesta.

Veľmi ma prekvapila miera **sociálnych akcií** (oprava: socializačných) – na tomto sme sa zhodli viacerí nováčikovia. Vedel som, že to tu žije, ale nečakal som, že prvé dva týždne tu bude nesmierne veľa akcií, či už z college, fakulty, univerzity alebo aj iných spolkov. Napríklad moja college (Kellogg) organizovala tour po Oxforde, pub crawl, večery hier, zadarmo brunch (breakfast + lunch), či formálnu večeru. Je to fakt super, lebo to dáva priestor spoznať nových ľudí, všetci sú tu milí a búra to samotu, ktorá, aspoň tak to vnímam, je na iných unverzitách častá. Na druhej strane ma už unavovalo mať stále small talk, kde som hovoril tie isté **4 informácie:** *„Pali, MSc Maths and Computer Science,  Kellogg college, Slovakia.“ – „Sorry again your name?“ – „Pa-li. Like palindrome but without the ndrome.“* 😁


<p class="image-in-blog">
<img style="max-width:500px;" src="/assets/images/newsletters/I/freshers_fair.jpg" alt="Fresher's Fair"/>
<em>Fresher's Fair</em>
</p>

Keď som pri socializácií, nemôžem nespomenúť, že pár dni pred začiatkom trimestra bol jeden skvelý event zvaný **Fresher’s Fair**. Vo veľkom stane bolo niekoľko sto stánkov rôznych spolkov riadených študentami do ktorých sa študenti môžu zapájať, napr. Physics Society, Tolkien Society, Czech&Slovak Society, Wine Society, Philosophy Society, Quidich (metlobal) Society, Dancing Society… Jednoducho, je toho viac ako by človek stihol robiť a aj preto je Oxford tak dobrý. Mnohí absolventi mi radili aby som nepodcenil sociálny život ale využil tieto príležitosti, lebo vďaka nim budem na tento rok dobre spomínať. Ja už som sa stihol prihlásiť na fakt asi milión vecí, tak uvidím, pri čom vydržím. Najviac ma zatiaľ baví Oxfordská klasika – veslovanie. Na Temži pri východe slnka je to jednoducho romantika.


Čo sa týka nových ľudí, tak je tu veľa ľudí zo zahraničia, obzvlášť na mojej college, s ktorými som už mal rôzne zaujímavé rozhovory. So židom, hinduistom, o slobodnej vôli, o interupciách, o kresťanstve, o staro-angličtine, o politike… **Debatuje** sa tu veľa a dôkazom toho je, že v Oxforde funguje asi najlepší debatný klub na svete – Oxford Union. Odporúčam pozrieť YouTubový kanál.

<p class="image-in-blog">
<img style="max-width:500px;" src="/assets/images/newsletters/I/flat_dinner.jpg" alt="Moji spolubývajúci"/>
<em>Moji spolubývajúci</em>
</p>

Čo sa týka vyučovania, tak za prvé dva týždne môžem povedať, že sú tu fakt zaujímavé **predmety** a dosť dobrí ľudia. Kým na MatFyze som bol jeden z lepších, tu zažívam, že som priemerný a niekedy som úplne mimo aj keď ostatným príde látka zrozumiteľná. Jedna väčšia zmena je, že predmety sa vyučujú kratšie (8 týždňov), ale zato intenzívnejšie. To znamená, že zapísať si viac ako 4 predmety je veľmi ťažko zvládnuteľné. Ja som zatiaľ veľmi chtivý a tak chodím na 5 predmetov plus na dva predmety na filozofickej fakulte – uvidím, či sa mi to podarí udržať celé dva mesiace. 

Prekvapivé je, že zatiaľ sa mi nezdá kvalita prednášok nejak dramaticky lepšia ako na Matfyze – zväčša sú tu totiž skôr **výskumníci než učitelia**. Na druhej strane, možností kvalitného výskumu tu vnímam podstatne viac a celkovo je tu aj lepšia starostlivosť o študentov spolu s dobrou atmosférou na prelome vyučujúci-študent.

## Aké to je?

Od momentu, čo ma prijali som dostal veľa otázok typu **„Aké to je dostať sa na Oxford?“** Musím sa priznať, že som na túto otázku nikdy poriadne nevedel odpovedať. Tentoraz to skúsim príbehom.

Bolo to skoro presne rok dozadu keď som v rozhovore s mojou mentorkou rozoberal, čo ďalej a zrazu v miestnosti zaznelo slovo *„Oxford“*. Vtedy som sa iba zasmial s tým, že **Oxford určite nie je pre mňa.** Avšak keďže som vedel, že ľudia z môjho okolia sa tam dostali, tak som sa predsa len pozrel na stránku a našiel som program, ktorý ma extrémne očaril. O 3 mesiace neskôr som už pri počítači nervózne opravoval posledné gramatické chyby môjho motivačného listu a len tak tak stihol poslať platbu za moju prihlášku.

Nasledovalo niekoľko týždňov čakania na mail, počas ktorých som rozoberal alternatívy ak by ma neprijali – čo bol, samozrejme, pravdepodobnejší variant. Potom prišiel **deň D.** Bol som doma v izbe, keď som započul notifikáciu na mobile. Po prečítaní predmetu a prvej vety som iba vykríkol a bežal za bratom, ktorý bol jediný doma – a akurát na malej. Naša konverzácia prebehla asi takto: *„Samoooooo!!“ – „Čo jeeeeee???!!“ – „Dočúraj!!“ … *dramatická prestávka* … „PRIJALI MA NA OXFORD!!“* 😅

Ten večer a nasledujúce dni boli plné **radosti a bázne**. A to som ešte ani nevedel, koľkých malých zázrakov a obdarovaní budem súčasťou. Treba však povedať, že som neprežíval erupcie emócií. Oxford je stále iba škola a navyše sa mi vo všeobecnosti ťažšie uvedomuje realita, ktorú nevidím a nie som priamo súčasťou.

<p class="image-in-blog">
<img style="max-width:300px;" src="/assets/images/newsletters/I/flat_house.jpeg" alt="7 Bradmore Road"/>
<em>Náš flat a môj domov!</em>
</p>

Mohol by som preskočiť 6 mesiacov a rozprávať o príchode, no za toho pol roka sa toho odohralo celkom dosť, čo stojí za zmienku. Prijatie na Oxford pre mňa totiž znamenalo **splniť 3 podmienky** – získať financie, červený diplom a certifikát z angličtiny. Najprv sa mi zdalo to prvé ako nedosiahnuteľný cieľ a ľudia z môjho okolia potvrdia, že som z toho mal mierne depky. To sa však obrátilo, keď som našiel sponzorov, ktorý mi pokryli značnú časť nákladov; vtedy som pochopil, že dobrých ľudí a štedrosti je vo svete dostatok. Potom nasledoval ťažký boj s bakalárkou a štátnicami a napokon stresy, keď som test z angličtiny nedal na prvý raz.

Našťastie sa všetko podarilo a teraz som tu. Aké to teda je byť na Oxforde? Moja cesta sem ukazuje, že za 12 mesiacov som si na všetky tie „Oxfordské špecifiká“ postupne zvykal a tak som nezažil žiaden brutálny kontrast vo vnútornom prežívaní. Ale teda, je to iné a **je mi tu veľmi dobre**. Musím si to však často pripomínať a vracať sa k celému tomuto príbehu, lebo človek si veľmi rýchlo zvykne, hoc aj na nesamozrejmé veci. Niekedy sa iba tak krátko pozastavím a zahľadím na nejakú peknú budovu, na veslárov na Temži, na veveričky na stromoch, či sa započúvam do anglických hymnov. To je aj moje povzbudenie každému, kto číta tento newsletter: Všímať si **malé zázraky** bežného dňa a **byť vďačný** za to množstvo milostí, ktorými sme zavalení.



## Exkurz: Oxfordský Slovník

- **fresher**  –  nováčik, ktorý začína svoje štúdium v Oxforde

- **college** – doslova kolégium; každý študent patrí do nejakej college kde má sociálny život (potenciálne kamarátov) , ubytovanie, stravu, niektorí aj cviká; skrátka, zázemie pre študentov

- **term** – trimester; jesenný sa volá Michealmas, zimný Hillary a jarný Trinity

- **society**  –  spolok oficiálne rozoznávaný univerzitou

- **MSc** – titul Master of Science (Oxford má aj DPhil čo je vlastne PhD)

- **formal dinner** – trojchodová večera na college spojená s rôznymi tradíciami a slávnostným oblečením

- **gown** –  plášť, ktorý nosia študenti a akademici; každý stupeň vzdelania má iný typ gown

- **sub fusc** – slávnostný odev, ktorý sa nosí pri významných udalostiach ako je imatrikulácia, či niektoré formálne večere; pozostáva z tmavého obleku/sukne, bielej košele, gown a tmavej/bielej kravaty/motýlika/stužky.

- **MCR** – Middle Common Room; teleso slúžiace na socializáciu magistrov na college; bakalári majú Junior CR

- **matriculation** – tradičná slávnostná imatrikulácia zo začiatku Michaelmas term, na ktorej sú študenti oficiálne prijatí na univerzitu

- **torpids, eights** – súťaž vo veslovaní medzi collegami počas Hillary term (torpids) a Trinity term (eights)

- **Oxbridge**  –  slovná skratka; Oxford a Cambridge dokopy


29.10.2022