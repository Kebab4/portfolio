---
layout: post
title: Newsletter II.
subtitle: 
banner: "/assets/images/newsletters/II/rowing.jpeg"

---
*Časť veslárskej posádky Christ Church college novicov spolu s trénerom na záverečnej večeri trimestra*

**Po skúsenosti prvého trimestra v Oxforde sa pozerám späť, či už z osobnej perspektívy toho, čo som prežil a čo bolo pre mňa najhodnotnejšie, ale aj z pohľadu výučby a toho, čo nové a zaujímavé som sa naučil. A samozrejme nesmie chýbať exkurz.**

## Michaelmas term

Letenky sú kúpené, bakalári opustili Oxford už pred dvoma týždňami, skúšky mám čoskoro za sebou, vonku je typicky Anglicky – ideálny čas na bilancovanie trimestra!

Kým prvé týždne boli plné akcií, spoznávania nových ľudí, prechádzok a fantastického počasia, zvyšok termu bol omnoho viac monotónnejší, obyčajnejší a omnoho viac o učení. Prišli dni kedy som bol na dne a myslel som si, že to nezvládnem a celé failnem. Prišli dni, kedy mi strašne chýbali priatelia a kamaráti zo Slovenska.Všehovšudy bol však tento trimester veľmi dobrý. 

Som naozaj vďačný za svojich spolubývajúcich a mojich spolužiakov. Na našom byte sa často spolu rozprávame, niečo sa tu deje, alebo si len tak spolu navaríme. Nepovedal by som, že sú to priatelia, ale je fajn mať takéto zázemie, kde vie človek prísť unavený, kde si vie posťažovať, zasmiať sa alebo sa len zavrieť do izby.

Spolužiaci sú skvelí a trávime spolu veľa času, najmä vďaka tomu, že máme jednu miestnosť na matematickej fakulte iba pre seba a veľa sa tam spolu učíme a stretávame. Mám tušenie, že toto budú ľudia, s ktorými budem na konci roka najčastejšie.

Dve príhody za všetky: I. Sedeli sme spolu v pube a bavili sa o rozdieloch v jazykoch (o čom inom by sa matematici vo voľnom čase bavili) a dohodli sme sa, že keďže jedno slovo pre „čas“ je málo, budeme ich používať viac: kairos, tempet, chronos, reocair, time. Uvidíme, či nám to vydrží. II. Viete ako sú v Oxforde societies na všetko? A tak sme vo voľnej chvíli vymysleli, že budeme mať society o hľadaní nekonečna. Začneme od 1, 2, 3, … a na každom stretnutí budeme pokračovať až kým sa nedopočítame k nekonečnu. No čo, pridali by ste sa? Ak sa teraz pýtate, či robíme aj niečo seriózne, tak v ďalšej sekcií sú ukážky.

Minule som spomenul, že som začal veslovať a dnes je pravda, že v tom pokračujem a dúfam, že budem pokračovať aj naďalej. Súťažili sme na dvoch pretekoch a našej „crew“ sa podarilo vyhrať 3/5 duelov. Keble college nás vyradil v osemfinále. Bloddy Keble! Ale teda samotné tréningy boli fakt fajné a o to lepšie, keď máte aj vtipného „cox“ (to je ten čo je na konci a všetkých vás diriguje). Na veslovaní je zaujímavé, že musíte ísť všetci úplne rovnako a keď prehráte, neviete to hodiť na ostatných. 

<p class="image-in-blog">
<img style="max-width:600px;" src="/assets/images/newsletters/II/kilns.jpg" alt="Kilns"/>
<em>Čítanie úryvkov z Lewisových kníh v jeho dome na Kilns.</em>
</p>

**Najlepšie momenty** – púť z Oxfordu do Littlemore v predvečer sviatku Sv. Johna Henryho Newmana, rozhovory s Rose (dôchodkyňa katolíčka z Hong Kongu), vianočný večierok v dome C.S. Lewisa, víťazstvo nad St. Johns v totálnom lejaku, stretnutie s Maruškou (absolventka Oxfordu z Česka)

**Najlepšie návyky** – obedovanie na iných collegoch, prednášky z filozofie, applikácia OneSecondEveryDay, písanie denníka

**Návyky na ďalší trimester** – chodiť na prednášky z iných oblastí, vytvoriť debatnú skupinku, behať do parku, skôr vstávať a chodiť viac pešo

## Čo som sa, napríklad, naučil

Môj kurz má veľkú slobodu vo výbere predmetov, čo je veľká výhoda, no na druhej strane si človek potrebuje vybrať a teda aspoň trošku rozumieť, čo si vyberá, čo pri predmetoch s názvami ako „Representation Theory of Semisimple Lie Algebra“ nie je vôbec jednoduché. Napokon som však veľmi šťastný za svoj výber predmetov. Tu sú niektoré:

Teória kategórií – veľmi veľmi veľmi abstraktný predmet. V podstate je to abstrakcia abstrakcií. Kategória spočíva v objektoch a šípkach medzi nimi. Potom existuje funktor – to je šípka medzi kategóriami. A potom existuje prirodzená transformácia – to je šípka medzi funktormi.  A potom sa pozeráte, ako tieto veci vedia spolu „komutovať“.


<p class="image-in-blog">
<img style="max-width:200px;" src="/assets/images/newsletters/II/category.png" alt="category theory"/>
<em>„prirodzenosť“ podľa teórie kategórií</em>
</p>


Aké jednoduché, však? (ha) Prekvapivo, má táto oblasť zaujímavé praktické využitie a aj filozofickú stránku. Jedno veľmi technické tvrdenie (Yoneda lemma) ľudskou rečou hovorí, že objekt je identifikovateľný všetkými šípkam, ktoré do neho vchádzajú (je identický tomu aké má „vzťahy“ s inými objektami). Musím sa však priznať, že tento predmet som nedokončil. Nestíhal som všetky predmety a tak som sa rozhodol ho „dropnúť“, ale určite sa k tomu ešte vrátim.

Teória modelov – konečne nejaká zaujímavá logika, ktorá nie je triviálna! Väčšinou sa človek o logike naučí najzákladnejšie základy, ktoré sú však (zväčša) zjavné každému premýšľajúcemu človeku, alebo aspoň dosť ľahké. Tento kurz (a celkovo oblasť výskumu teórie modelov) je o dualite teórií a modelov. Teória je súbor logických výrokov, ktorým chýba význam. Model je interpretácia jednotlivých výrokov a teda aj intepretácia teórií. Dokopy sa dopĺňajú. Takže je nezmyselné povedať „Tamtá teória je pravdivá“ ale „Tamtá teória je pravdivá v tomto modeli.“ Upozornenie, slová teória a model tu majú veľmi špecifický význam iný od toho bežného (napr. od fyziky). Ja už som si zvykol, že názvy v matematike žijú svojím životom. Napríklad toto  je tropický polokruh, lebo chlapík, čo to skúmal, pracoval v Brazílii.

Kvantové procesy a výpočty – nie neštudujem fyziku, no už v minulom storočí sa informatikom podarilo ukázať, že kvantové javy sa dajú využiť na modelovanie a počítanie ťažkých problémov. A tak sa zrodil koncept kvantového počítača (zatiaľ sú všetky veľmi malé, ale ak sa raz podaria väčšie, bude to vzrúšo). Prekvapenie: na tomto kurze sme veľa kreslili, lebo je vlastne o diagramatickom prístupe k celej oblasti kvantových procesov. Takže napríklad takto vyzerá riešenie jednej z domácich úloh. Cool, nie?


<p class="image-in-blog">
<img style="max-width:300px;" src="/assets/images/newsletters/II/quantum.png" alt="quantum"/>
<em>Tie biele a čierne bodky voláme „pavúci“ </em>
</p>


Filozofia matematiky – v pondelky ráno sme sa so spolužiakmi stretávali, aby sme si vypočuli hodinkovú prednášku na zaujímavú tému, napríklad: A) Vieme experimentálne vyvrátiť matematickú teóriu, ktorá bola riadne dokázaná? Čo ak zoberieme košík s 12 hruškami a košík s 13 hruškami a keď ich dáme dokopy, tak, čo počítame to počítame, vychádza nám, že máme 26 hrušiek. Vyvrátili sme tým aritmetiku? B) Skoro neustále využívame tzv. zákon vylúčenia tretieho: Ak ukážem, že nie je možné, aby som mal súrodencov, tak som dokázal, že nijakého nemám. Očividné! Intuicionisti by však so mnou nesúhlasili.


## Exkurz: Ox-mas

V poslednej štvrtine roka sú v Anglicku štyri významné sviatky: Halloween (31.10.), Bonfire Night (5.11.), Remembrance Day (11.11) a Vianoce. Bonfire Night je pripomienka toho, ako sa skupinke katolíkov v 17. storočí nepodarilo zapáliť parlament a v tento deň sú každý rok masívne ohrňostroje a ohňe. Potom nasledujúci týždeň nosia všetci červené maky na spomienku padlým v Great Wars. 

Akonáhle toto všetko skončí, začne sa Christmas Vibe: všade sú vianočné ozdoby, vešajú sa pouličné dekorácie a onedlho začnú ľudia organizovať spievanie Carols (kolied). Keďže však veľká časť študentov odcházda z mesta relatívne skoro (8. týždeň tento rok skončil 2.12.), tak všetci chcú mať vianočné večierky, spievanie kolied, tajných santov a podobné veci skôr než odídu. Tieto skoré Vianoce sa tu volajú aj Ox-mas. 

<p class="image-in-blog">
<img style="max-width:300px;" src="/assets/images/newsletters/II/christmas_bod.jpg" alt="old bod christmas"/>
<em>Vianoce dokonca aj v Old Bodleain Library.</em>
</p>



Musím povedať, že spievať „Glory to the new born King“ v NOVEMBRI bolo na mňa proste moc. Ale veľmi sa mi páči, ako tu chcú všetci oslavovať a uvedomil som si, že, aspoň podľa mňa, u nás nie je spievanie kolied taká kultúra. Tiež tu je zvykom na vianočných večierkoch vybuchovať také tie veľké salónky (crackers). Vytvorí sa reťaz z rúk a každý naraz potiahne jeden z koncov salóniek, v ktorých je vždy niečo zaujímavé, ako vtip, dekória, a samozrejme, koruna z farebného krepového papiera, ktorú potom nosíte celý večer na hlave.

<p class="image-in-blog">
<img style="max-width:500px;" src="/assets/images/newsletters/II/christmas_dinner.jpg" alt="christmas dinner"/>
<em>Vianočná večera na college.</em>
</p>




18.12.2022