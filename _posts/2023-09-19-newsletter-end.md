---
layout: post
title: Newsletter IV.
subtitle: 
banner: "/assets/images/newsletters/IV/punt.jpg"

---
*Kapitán J vyzerajúci prístav počas najtypickejšej letnej aktivity v Oxforde – punting (sl: loďkovanie)*

**Voda, ktorá napršala počas tretieho trimestra či počas leta už dávno odtiekla a to mi umožňuje pozrieť sa definičné momenty týchto období – návštevy mojich kamarátov v Oxforde a písanie mojej diplomovej práce. Zároveň zodpoviem najťažšiu otázku o mojom štúdiu a tým aj ukončím túto sériu newslettrov.**

## Rozširovanie obzorov
Zdalo by sa, že leto, počas ktorého budem písať svoju diplomku, nebude veľmi záživné. To som ešte nevedel kde všade sa ocitnem a kto všetko ma príde navštíviť priamo do Oxfordu. Pri piatej **návšteve** sa ma už spolubývajúci a spolužiaci začali podozrivo pýtať, že koľko ešte ďalších ľudí príde. Hlavne počas mája a júna som sa stal hostiteľom skoro každé dva týždne. Bolo to časovo náročné, ale som veľmi rád, že tých zopár ľudí mohlo prísť, vidieť najstraršiu britskú univerzitu zvnútra, ale hlavne vidieť, ako som v nej ja žil. Niektoré veci, ako napr. študentský život, či systém collegov sa ťažko predstavujú kým človek nepríde.

<p class="image-in-blog">
<img style="max-width:300px;" src="/assets/images/newsletters/IV/breakfast.jpg" alt="breakfast"/>
<em>Kellogg cornflakes na raňajky v Kellogg college</em>
</p>

Zároveň bolo skvelé sledovať ako ľudia komentujú niektoré veci, na ktoré som si zvykol, alebo si ich ani nikdy nevšimol. Z tých zopár pozorovaní vyberám:

Kým na Slovensku máme zvyk historické budovy izolovať a pred človekom chrániť, v Oxforde  sa mimoriadne **historické budovy stále využívajú** na účely, na ktoré boli postavené – knižnice, ubytovania, jedálne…

Ďalšou vecou je, že skoro každá college má buď nejaký extrémne **zelený trávniček** (po ktorom sa väčšinou nedá kráčať), alebo aspoň masívnu záhradu, ktorá sa viac ponáša na botanickú záhradu než na park pre ubytovaných študentov.


<p class="image-in-blog">
<img style="max-width:300px;" src="/assets/images/newsletters/IV/edmund.jpg" alt="stedmunds"/>
<em>Vistériová ozdoba St. Edmunds college</em>
</p>

Zaujímavé bolo tiež to, že s výnimkou jedného dňa som s návštevami nikdy nepoužil **žiaden verejný dopravný prostriedok** a aj tak nám zväčša zostalo kopec vecí, ktoré by sa dali v centre pozrieť. Poukazuje to na to, aké je Oxford malé a husté mestečko. Napr. existujú v ňom dve dobré múzeá (Ashmolean Museum a Natural History Museum), ktoré som ani po celom roku nestihol poriadne prekutrať.

Pokračujúc v téme návštev, v júli sa mi podarilo ísť na jednu „návštevu“ do Nemecka. Konkrétne som išiel na **vedeckú konferenciu z informatiky**, na ktorej bol prezentovaný článok, na ktorom som spolupracoval minulé leto v IST Austria. Čiže keď si v google scholar vyhľadáte moje meno, dostanete nenulový počet výsledkov. Inak povedané, mám naštartové na vedeckú kariéru. 😉


## Diplomka

Od apríla do konca augusta som pracoval na svojej diplomovej práci, ktorú idem zajtra (20.9.) obhajovať. Aj keď téma nebola zrovna z oblasti, ktorá by ma extra nadchýnala, mal som obrovské šťastie na školiteľa a napokon aj na samotnú tému, keďže sa môj výskum (dúfajme) pretaví do článku na vedeckej  konferencii. Nielenže bol môj školiteľ profesor (často je školiteľom buď PhD študent alebo postdoc), ale navyše som sa s ním stretával niekedy aj dva krát za týždeň, čo je pri vyťažených profesoroch ako je on veľmi výnimočné. Téma bola z prieniku oblastí: **teória čísel a kombinatorika.** Teória čísel ma vždy lákala v tom, že číselné sústavy, prvočísla, či deliteľnosť sú úžasne fascinujúce a pritom veľmi prirodzené javy. Je to vlastne rozšírená „aritmetika“. Kombinatorika je  zas veľmi hravá či ľahko predstaviteľná a otvorených problémov je v nej neúrekom.

Ak vás zaujíma o čom som písal, napísal som **dva blogy, ktoré sa snažia moju prácu priblížiť.** [V prvom](https://tinyurl.com/japries) vysvetľujem ako fungujú priestupné roky a tým približujem kombinatorickú časť mojej práce. [V druhom](https://tinyurl.com/jatran) rozprávam o transcendentných číslach a motivujem časť mojej práce, ktorá sa zaoberá teóriou čísel. Obe som sa snažil napísať tak, aby boli pochopiteľné pre nematematika a aby obsahovali aj zopár „domácich úloh“.


## Stálo to za to?

Nie je to pre mňa ľahká otázka najmä preto, že v stávke toho nebolo málo. Po tomto roku som však presvedčený, že odpoveď je *áno* a že ak by som vedel, čo ma čaká, *išiel by som do toho znova.* Moju analýzu skúsim rozdeliť do niekoľkých oblastí:

**Akademické vzdelanie**

Tu som bol mierne sklamaný, keďže som rýchlo pochopil, že v oblasti STEM (science, technology, engineering, mathematics) je Oxfordská univerzita viac akadémia ako univerzita. Teda sa viac zameriava na to, aby mala špičkových vedcov než aby mala dokonalý systém vyučovania. V tomto sa zhodujem aj s inými študentami, ktorí vnímali, že prednášajúci určite nie sú crème de la crème svetových prednášajúcich. V mojom prípade som mal však dve výhody. Po prvé, študujem *magisterský program*, ktorý je skôr nadstavbou a teda vďaka bakalárskemu štúdiu už dokážem pracovať viac-menej samostatne a učiť sa zo skrípt a s pomocou iných študentov. Po druhé, mal som obrovské *šťastie na dobrých spolužiakov* z MFoCS. Ťahali sme sa, vysvetľovali sme si veci, motivovali sme sa v poznávaní ďalších oblastí matimatiky, trávili sme spolu veľa času mimo prednášok a cvík. Takáto skúsenosť je výnimočná vo všeobecnosti.

**Iné vzdelanie**

Každá prestížna univerzita má výhodu v tom, že sa v nej nachádzajú veľmi *inteligentní ľudia* a pozývajú sa tam *veľmi vzácni hostia.* Túto výhodu som nedokázal využiť úplne naplno, ale rozhodne som si rozšíril obzory a získal záujem o ďalšie oblasti. Doteraz mám v živej pamäti semináre o Wittgensteinovej filozofii jazyka, debatu so študentkou teológie hinduizmu, nekončiace rozhovory o rozdielnosti  nemčiny vs. francúzštiny vs. angličtiny vs. tamilčiny vs. slovenčiny,  či zisťovanie aký bol študentský život v stredoveku. 


<p class="image-in-blog">
<img style="max-width:300px;" src="/assets/images/newsletters/IV/lecture.jpg" alt="lecture"/>
<em>Prednáška o filozofických dôsledoch Gödelových viet</em>
</p>

**Študentský život**

Oxford je veľmi medzinárodné a študentské mestečko a to so sebou nesie výhody, o ktorých by vedeli rozprávať všetci, ktorí boli na nejakom Erazme či študovali v zahraničí. Oxford je špecifickejší v tom, že má *systém collegov*, ktorý sa postaral napr. o to, že som mal  komunitné zázemie, spoznal som viac ľudi, mal som dobrú stravu, viac priestorov na štúdium a viac úradníkov, ktorí sa vedeli postarať o moje potreby. Avšak hlavná výnimočnosť bola *rozmanitosť študentských spolkov*, o ktorej som nepočul, že by bola kdekoľvek inde. Konkrétne som si našiel niekoľko intelektuálnych a športových klubov, do ktorých som sa zapájal.

Dohromady teda poviem, že to bol veľmi dobrý rok a to najmä vďaka variabilite dobier, z ktorých som počas roka čerpal. Samozrejme, posunul som sa aj osobnostne, ale o tom sa ľahšie rozpráva v osobnom rozhovore.

Moja skúsenosť je zaujímavá v tom, že som si zažil slovenskú univerzitu, anglickú univerzitu a aj „slovenský Oxford“ (Kolégium Antona Neuwirtha). Rád teda pomôžem, ak bude niekto potrebovať poradiť s výberom školy, avšak musím poznamenať, že moja skúsenosť sa týka hlavne STEM.  Viem teda povedať, že bratislavský Matfyz je fakt skvelý na získanie základného vzdelania, ale vo výskume má ešte rezervy. Pri humanitných vedách, medicíne, práve, či umeleckých smeroch budú reálie veľmi iné.

**Návyky z predošlého trimestera** – obdivovať parky a načúvať vtáčikom 👍, každý týždeň navštíviť inú society 👎, zapojiť sa do rugby alebo behať 🫳, chodiť na prednášky z iných oblastí 👎.

## Exkurz: Architektúra

Oxford je architektonicky tak zaujímavé miesto, že aj na človeka ako som ja sa niečo nalepilo. Vďaka tomu, že vojny toto mesto prakticky obišli, sa zachovalo úžasne veľa budov, takže človek má pri prehliadke Oxfordu prakticky aj prehliadku vývoja architektúry v Európe. Na nasledujúcej strane ponúkam prehľad mojich obľúbených miest či prvkov, ktoré som počas roka obdivoval. Veľmi som si užíval rôznorodosť a veľkoleposť knižníc, organov, portálov, drevorezieb, či vitráží na rôznych collegoch. Štvrť, kde som býval, bola postavená vo viktoriánskom štýle a bola to veru radosť robiť si v nej len tak prechádzky. Z prechádzok som mal rád aj uličku vedľa Keble college. Obvzlášť poobede, keď na kaplnku svietilo ostré slnko. Nedeľným domovom sa mi stal oratoriánsky kostol St. Alojza a podobný nedeľný „vibe“ majú aj záhrady Lady Margaret Hall. Majestátnosť človek pocíti vždy keď ide popod Tom Tower či keď prechádza južným krídlom matematickej fakulty. Zámerne som vynechal hlavné či turisticky najfotenejšie dominanty.

<p class="image-in-blog">
<img style="max-width:500px;" src="/assets/images/newsletters/IV/kniznica.jpg" alt="kniznica"/>
<em>Knižnica Christ Church college, 18. st.</em>
</p>
<p class="image-in-blog">
<img style="max-width:500px;" src="/assets/images/newsletters/IV/bradmore.jpg" alt="bradmore"/>
<em>Výhľad z môjho okna – Bradmore Road, 19. st.</em>
</p>
<p class="image-in-blog">
<img style="max-width:300px;" src="/assets/images/newsletters/IV/natural.jpg" alt="natural history museum"/>
<em>Natural History Museum, 19. st.</em>
</p>
<p class="image-in-blog">
<img style="max-width:300px;" src="/assets/images/newsletters/IV/keble.jpg" alt="keble"/>
<em>„Kaplnka“ Keble college, 19. st.</em>
</p>
<p class="image-in-blog">
<img style="max-width:300px;" src="/assets/images/newsletters/IV/organ.jpg" alt="organ"/>
<em>Organ Merton college, 2013</em>
</p>
<p class="image-in-blog">
<img style="max-width:300px;" src="/assets/images/newsletters/IV/door.jpg" alt="door"/>
<em>Merton college, 13. st.</em>
</p>
<p class="image-in-blog">
<img style="max-width:500px;" src="/assets/images/newsletters/IV/floor.jpg" alt="maths floor"/>
<em>Mathematical Institute, 2013</em>
</p>
<p class="image-in-blog">
<img style="max-width:300px;" src="/assets/images/newsletters/IV/tom.jpg" alt="tom tower"/>
<em>Nepostrádeteľná veža „Tom“, 17. st.</em>
</p>
<p class="image-in-blog">
<img style="max-width:500px;" src="/assets/images/newsletters/IV/coloured2.jpg" alt="coloured street"/>
<em>Farebná Observatory Street, 19. st.</em>
</p>
<p class="image-in-blog">
<img style="max-width:500px;" src="/assets/images/newsletters/IV/lmh.jpg" alt="lmh"/>
<em>Lady Margaret Hall, 19. st.</em>
</p>
<p class="image-in-blog">
<img style="max-width:500px;" src="/assets/images/newsletters/IV/mansfield.jpg" alt="mansfield"/>
<em>Drevorezba v kaplnke Mansfield college, 19. st.</em>
</p>
<p class="image-in-blog">
<img style="max-width:300px;" src="/assets/images/newsletters/IV/worcester.jpg" alt="worcester"/>
<em>Sultan Nazrin Shah Center, Worcester college, 2017</em>
</p>
<p class="image-in-blog">
<img style="max-width:300px;" src="/assets/images/newsletters/IV/stained.jpg" alt="stained"/>
<em>Vitráž kaplnky New college, 18. st.</em>
</p>
<p class="image-in-blog">
<img style="max-width:300px;" src="/assets/images/newsletters/IV/oratory.jpg" alt="oratory"/>
<em>Kostol sv. Alojza Gonzaga, 19. st.</em>
</p>
<p class="image-in-blog">
<img style="max-width:500px;" src="/assets/images/newsletters/IV/maths.jpg" alt="maths"/>
<em>Južné krídlo Mathematical Institute, 2013</em>
</p>
<p class="image-in-blog">
<img style="max-width:500px;" src="/assets/images/newsletters/IV/stanthonys.jpg" alt="st anthonys"/>
<em>St. Anthony’s college, 20.st.</em>
</p>














19.9.2023