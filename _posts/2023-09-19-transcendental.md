---
layout: post
title: Transcendentné čísla a moja diplomka
subtitle:
banner: "/assets/images/blog/transcendence/banner.jpg"
---

"transcendentné"

*KSSJ*: zmyslami nepostihnuteľný, nadzmyslový, nadprirodzený

*Oxford dictionary*: idúce za hranicu bežných limitov ľudského poznania, skúseností a rozumu; obzvlášť v náboženskej alebo duchovnej forme

*Cambridge dictionary*: udalosť, objekt alebo idea, ktorá je extrémne špeciálna, výnimočná a nedá sa pochopiť obyčajným spôsobom

Mám pre vás **niekoľko úloh**, nad ktorými sa zamyslite a vyskúšajte si ich:

1. Máte k dispozícií **číslo 1 a operáciu sčítania.** *Ktoré všetky čísla viete dostať použitím iba týchto dvoch vecí?*
Napríklad číslo 3 = 1+1+1. Číslo 12 = 1+1+1+1+1+1+1+1+1+1+1+1. Nie je ťažké uvedomiť si, že toto sú všetky prirodzené čísla. A aj preto majú meno prirodzené.

2. Teraz máte k dispozícií **číslo 1, operáciu sčítania a operáciu odčítania**. Aké všetky čísla pomocou nich viete dostať? Viete dostať 0? Alebo 2/3? Žeby -1000? Čo takto $\pi$? A zlatý rez $\frac{1 + \sqrt{5}}{2}$ by nešiel? Odpoveď: alsíč élec

3. Čo ak pridáme **operáciu násobenia**? Odpoveď: élec abi eláts

4. Pridajme ešte **operáciu delenia**. Teda už máme číslo 1, plus, mínus, krát, deleno. Ktoré všetky čísla vieme dostať? Odpoveď: ykmolz yktešv adet alsíč enlánoicar. Bonus: Čo ak môžete použiť operáciu delenia iba raz?

5. Napokon pridajme aj **operáciu odmocnenia**. Napr. druhá odmocnina čísla 2 je $\sqrt{2}$ a tretia odmocnina čísla $\sqrt[3]{125} = 5$. Čo všetko vieme vyjadriť teraz? Odpoveď: alsíč ékciarbegla

Tieto otázky sú pre matematiku veľmi fundamentálne, lebo sa zaoberajú základnými limitami nášho chápania čísel a ako s nimi narábame. Ak poznáte odpoveď na 5. úlohu, viete, že všetky čísla, ktoré vieme dosiahnuť danými operáciami sa volajú **algebraické**. (Slovo "algebra" pochádza z arabčiny a znamená "napravovať", doslova "napravovať kosti".) Ukazuje sa, že *aj keby sme pridali všetky rozumné konečné operácie, stále budeme mať iba algebraické čísla.* A tým prichádzame do sveta "za algebrou", teda do sveta **transcendentných čísel.**


<p class="image-in-blog">
<img style="max-width:600px;" src="/assets/images/blog/transcendence/cisla.png" alt="Hierarchia čísel"/>
<em>Od prirodzených po algebraické čísla.</em>
</p>

Ako vôbec také transcendentné číslo vyzerá? **Existujú vôbec?** Táto otázka bola na stole veľmi dlho, ale vyriešila sa až v 19. storočí (teda relatívne nedávno), keď sa konečne dokázalo, že Ludolfovo číslo $\pi$ a Eulerovo číslo $e$ nie sú algebraické, teda sú transcendentné. Kto už niekedy narazil na Taylorove rady vie, že čísla $\pi$ a $e$ dokážeme iba approximovať a presnú hodnotu dosiahneme sčítaním nekonečne veľa čísel.

$$\frac{\pi}{4}  = 1 - \frac 13 + \frac 15 - \frac 17 + \frac 19 \cdots$$

Za posledných dvesto rokov sa vyvinulo niekoľko metód na dokazovanie, že nejaké číslo je transcendentné. Prvú metódu priniesol Liouville v roku 1844 a dokázal niečo na štýl: *"ak sa dá iracionálne číslo **veľmi dobre approximovať** racionálnymi číslami, tak je transcendentné"*. Pozorný čítateľ si všimol, že záludnosť tejto vety spočíva v slovnom spojení *"veľmi dobre"*. Liouville-ova metóda bola veľmi prísna a iba pár čísel prešlo jeho testom *"veľmi dobre"*. Neskôr však matematici ako Roth či Schlickewei trošku poľavili a tak máme podstatne viac čísel, ktoré vedia podliezť laťku *"veľmi dobre"*. Viac v [2. kapitole a 2. sekcii mojej diplomky.](/assets/files/diss.pdf) Metód sa už odvtedy našlo viac, hoci Liouville-ova je v niečom veľmi intuitívna a hovorí nám toto:

**"Ak si zoberiete desatiný zápis nejakého čísla, a nájdete tam ľubovoľne dlhé opakovania, tak to číslo je buď racionálne alebo transcendentálne."**



<p class="image-in-blog">
<img style="max-width:200px;" src="/assets/images/blog/transcendence/liouville.jpeg" alt="Liouville"/>
<em>Joseph Liouville 1809-1882</em>
</p>

Toto nás ultimátne vedie k svätému grálu transcendentnej teórie čísel, teda k vete, ktorú chce každý dokázať:

**"Všetky algebraické iracionálne čísla (teda čísla ktoré nie sú ani racionálne, ani transcendentné) sú normálne, i.e., vyzerajú náhodne."**

Číslo je *normálne* vtedy, keď sa v ňom náchadza každá postupnosť cifier nekonečne veľa krát a s rovnomernou frekvenciou. Veľa ľudí verí, že číslo $\pi$ je normálne. To by znamenalo, že v ňom viete nájsť svoje telefónne číslo, riešenie vášho sudoku, či celého Hamleta (ľubovoľne zakódovaného). Viď [searching names in pi.](https://www.atractor.pt/mat/fromPI/PIalphasearch-_en.html)

Istotne sa teraz niekto z druhej strany stola s neistým hlasom spýta: "A... načo je toto všetko dobré?"

Ja si odchlipnem z už-nie studeného Rízlingu a spustím: "Nuž, to asi závisí od toho, čo všetko môže byť dobré. [Sú krásne veci dobré?](https://kolegium.org/transcendentalia/) Je poznanie prvých príčin dobré? Ak je aspoň na jedno z toho odpoveď áno, tak si myslím, že aj zdanlivo neaplikovateľná matematika je dobrá. Aritmetika, geometria či astronómia patrili v stredoveku k slobodným umeniam -- slobodným od užitočnosti a od potreby praxe -- a to práve preto, že **sú fundamentálne** asi tak, ako fyzika. A kto by povedal, že poznať, aké zákony hýbu naším svetom je zbytočné?!"

Trošku som sa odtrhol od reťaze. V skutočnosti existujú aj celkom zaujímavé aplikácie teórie transcendentných čísel v teórií zložitosti (pre záujemcov posúvam [tento článok](https://epubs.siam.org/doi/pdf/10.1137/1030134?casa_token=Sp3SpHrzI48AAAAA:t9PqZHheDr_Nyd5ozyElF5isXQBxk8GqmESoRJkSUVOe_3w1pGTPaPy0r23PpKRRiaIKsMXG5Tf7)). 

V mojej diplomke som prišiel **s novou metódou na hľadanie transcendentných čísel** na základe ich desatiného zápisu ([kapitola 3. sekcia 6.](/assets/files/diss.pdf)). Konkrétne, ak si zoberiete číslo (napr. $\pi$) a vezmete si onoho desatiný zápis ($\pi = 3.14159265358...$), ak táto postupnosť (teda $314159265358...$) obsahuje veľa úsekov, ktoré sa opakujú, hoci aj s malou chybičkou, tak dané číslo (teda napr. $\pi$) je transcendentálne. Číslo $\pi$ tu bolo iba ilustratívne a reálne som moju metódu naň neaplikoval. Veľmi pravdepodobne by ani nefungovala. Ale našiel som iné čísla, pre ktoré som dokázal, že sú transcendentné. Konkrétne čísla, ktorých desatiný zápis je Sturmové slovo a dokonca aj ich nadmnožinu -- Episturmové slove. Viac o týchto slovách si viete prečítať v [tomto blogu.](/2023/09/19/priestupne.html)

Takže namiesto toho, aby ste si pozreli [B-čkový film](https://www.youtube.com/watch?v=VCTen3-B8GU&ab_channel=RottenTomatoesTrailers), vám radšej odporúčam nájsť, kedy má niekto najbližší narodeniny a [nájsť ich meno](https://www.atractor.pt/mat/fromPI/PIalphasearch-_en.html) v čísle $\pi$ 😉

