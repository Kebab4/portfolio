---
layout: post
title: Coming soon!
subtitle:
banner:
---

Už dlhšie sa chystám písať blogy, tak tu je nástrel tém, ktoré sa sem snáď raz dostanú:

### Oxford
- Mesačný newletter (<a href="/newsletter.html">Prihláste sa na odber</a>)
- Deň v živote študenta
- ...

### Vedy
- O absolútnosti pravdy matematikovej
- Typy matematikov
- ...

### Filozofia 
- Sloboda = náhoda?
- Hlavné kritérium hodnotenia filozofie: žiteľnosť alebo logickosť?
- ...

### Teológia
- Milosť posväcujúca - diskrétna či spojitá?
- Prvá mnohosť a Trojica
- ...


### Osobné
- Dva roky v Kolégiu
- Hory a samota
- ...
