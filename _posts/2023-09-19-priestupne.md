---
layout: post
title: Priestupné roky a moja diplomka
subtitle:
banner: "/assets/images/blog/leap_year/banner.jpg"
---

"Neviem, či ma klame kalendár. Veď na ňom je 30. február!"

Aj takto znie refrén [piesne](https://www.youtube.com/watch?v=QsOfSKCZzds&ab_channel=In%C3%A9Kafe-Topic) od kapely Iné Kafe.
Ja som 30. február v kalendári ešte nevidel, ale na 29. február som už to šťastie mal. A dokonca sa mi pošťastilo stretnúť človeka, ktorý sa v ten deň narodil! Keď takého stretnete, spýtajte sa ho kedy oslavuje narodeniny. (Zároveň by vás to malo primúť zamyslieť sa nad tým, či naozaj oslavujete svoje narodeniny v správny deň.)

Ja sa však nebudem zamýšľať nad 30. februárom (ktorý by existoval nebyť skúpeho Augustusa, ktorý ukradol jeden deň Februáru aby ho prilepil do svojmu mesiaca August) a ani sa nebudem zamýšľať nad krásami iných netradičných kalendárov, ktoré riešili priestupné roky celkom iným spôsobom. 
Všetky tieto veci sú pekné, avšak sú to ľudské modely riešenia problémov pevne daného solárneho systému. Totiž to, že každé štyri roky máme jedeň deň navyše síce navrhol Július Cézar, avšak urobil to preto, aby sa synchronizoval s tým, ako sa hýbe a točí planéta.

**Deň a rok** sú najprirodzenejšie časové jednotky našej planéty a ich pomer je zrejme najprirodzenejšia časové konštanta. Takzvaný "tropický rok" je dĺžka kým sa zem otočí okolo slnka a vráti sa na to isté miesto. Pomer roka a dňa je

1 rok = **365.2421897** ... dní

Čo to pre nás, smrteľníkov, znamená? Že, ak by ste sa rozhodli mať kalendár s fixným počtom dní 365, tak každé štyri roky by ste stratili jeden deň. Napríklad, ak by bol v nejakom roku letný slnovrat 22. júla, o 100 rokov by ste mali letný slnovrat 18. augusta. To asi nie je úplne dobrý návrh kalendáru.
Pre vizualizáciu pomôže tento obrázok.


<p class="image-in-blog">
<img style="max-width:500px;" src="/assets/images/blog/leap_year/tropical-year-illustration.png" alt="Leap year slnečná sústava"/>
<em>Ak má rok 365 dní, tak oproti tropickému roku nám chýba ešte približne štvrť dňa. Ak má rok 366 dní, tak oproti tropickému roku získame navyše približne 3/4 dňa.</em>
</p>

Ak by sme mali priestupný rok každé štyri roky, tak to znamená, že v priemere by jeden rok trval $365 + \frac 14 = 365.25$ dní. To nie je dokonalá aproximácia a **po nejakom čase sa to aj pokazí.** Veď si to aj všimli v 16. storočí, kedy sa im rok posunul oproti Juliánskemu kalendáru o viac ako 10 dní. Pápež Gregor XIII. vtedy zaviedol gregoriánsky kalendár. Tento kalendár stále používame a priestupné roky tento kalendár rieši takto:
* každé **4** roky sa pridá jeden deň (priestupný rok)
* avšak každých **100** rokov sa tento deň nepridá (nie je priestupný rok)
* avšak každých **400** rokov sa predsa len deň navyše pridá (priestupný rok)

<br>
Teda napríklad v roku 2000 priestupný rok bol a v roku 2100 priestupný rok nebude.
To znamená, že podľa gregoriánskeho kalendáru sa 29. február za 400 rokov objaví presne 97-krát. Teda gregoriánsky kalendár aproximuje jeden rok na 

$$ 365 + \frac {97}{400} = 365.2425$$

To vyzerá ako dostatočne dobré, ale **po nejakom čase sa aj toto pokazí.** Presnejšie, každých 3,200 rokov sa nám kalendár posunie o jeden deň. Nasledujúca grafika z wikipédie (https://en.wikipedia.org/wiki/Leap_year) krásne ukazuje ako sa posúva letný slnovrat vzhľadom na dátum podľa toho, či bol a či nebol priestupný rok. Odporúčam sa k nej vrátiť po prečítaní tohto blogu; možno vám bude zrozumiteľnejšia.

<p class="image-in-blog">
<img style="max-width:700px;" src="/assets/images/blog/leap_year/leap_shift.png" alt="Leap year shift"/>
<em>Malé zúbky sú dôsledok toho, že každé 4 roky je priestupný rok; každých 100 rokov je skok pretože sa priestupný rok vynecháva (okrem rokov deliteľných 400, čiže roka 2000).</em>
</p>

Človeku napadne otázka: nemôžeme to vyriešiť natrvalo nejakým jednoduchým opakovaním? Odpoveď vás nepoteší, ale bude zaujímavá a privedie nás zadnými dverami k mojej diplomke. **Neexistuje totiž žiadne jednoduché opakovanie.** Gregoriánsky kalendár používa 3 pravidlá, ale aj keby sme ich mohli mať 10, či milión, po nejakom čase by sa náš kalendár predsa len desynchronizoval (poznámka pre pokročilých: tu predpokladáme, že pomer dĺžky dňa a roka je iracionálne číslo).

Problém kedy dať roku 365 dní a kedy 366 dní je vlastne **problém aproximácie** vyššie uvedeného čísla 365.2421897.... Verte mi, v živote aproximujeme veľa a neuvedomujeme si to. Zopár príkladov:

- Šoférujete auto a chcete vytočiť zákrutu, ktorá ide doľava. Natočíte volant do dobrého uhlu, avšak po chvíli začínate vchádzať do susedného pruhu naľavo. Zmeníte trošku držania volanta aby ste išli doprava, ale zrazu začínate ísť k zvodidlám. Opäť trošku natočíte volant doľava, ale teraz to už robíte jemnejšie. Atď.

- Automatický radiátor/klimatizácia v izbe, ktorá sa potrebuje spustiť v správnom čase a v správnej intenzite, aby udržovala viac-menej stabilnú teplotu.

- Asi by sa našlo celkom veľa vecí z tzv. [control theory](https://en.wikipedia.org/wiki/Control_theory), ale nie som inžinier, tak mi odpusťte.

Niektoré (vôbec nie všetky) problémy aproximácie sa dajú zredukovať na hľadanie nasledujúcich postupností:

*Majme nejaké kladné reálne číslo menšie ako 1 a nazvime ho $\alpha$. Napr. $\alpha = 0.2$. Taktiež majme nejaké číslo, ku ktorému budeme niečo pripočítavať -- nazvime ho $S$ a nastavme $S = 0$. Donekonečna budeme opakovať toto:*

*1. K číslu $S$ **pripočítajme $\alpha$**, i.e., $S \rightarrow S + \alpha$*

*2a. Ak $S$ práve prekročilo nejaké celé číslo, zasvietime **zelené svetlo**.*

*2b. Inak zasvietime **červené svetlo**.*

Teda ak predpokladáme $\alpha = 0.2$, postupnosť hodnôt bude

| číslo kroku         | S     | prekročilo sa celé číslo? |
|--------------|-----------|------------|
| 1. | 0.2      | 🔴        |
| 2. | 0.4      | 🔴        |
| 3. | 0.6      | 🔴        |
| 4. | 0.8      | 🔴        |
| 5. | 1      | 🟢        |
| 6. | 1.2      | 🔴        |
| 7. | 1.4      | 🔴        |
| 8. | 1.6      | 🔴        |
| 9. | 1.8      | 🔴        |
| 10. | 2      | 🟢        |
| 11. | 2.2      | 🔴        |
| ... | ...      | ...        |

Nie je ťažké všimnúť si, že vzor 🔴🔴🔴🔴🟢 sa bude opakovať donekonečna. Tu sme počítali s $\alpha = 0.2$, ale čo ak by sme zvolili iné číslo, napr. $\alpha = 2/3 \approx 0.666$?

| číslo kroku         | S     | prekročilo sa celé číslo? |
|--------------|-----------|------------|
| 1. | 0.666...      | 🔴        |
| 2. | 0.333...      | 🟢        |
| 3. | 1      | 🟢        |
| 1. | 1.666...      | 🔴        |
| 2. | 1.333...      | 🟢        |
| 3. | 2      | 🟢        |
| ...| ...| ...|

Opäť si všimnime, že tu máme postupnosť, ktorá sa opakuje, konkrétne 🔴🟢🟢. Ako cvičenie sa môžete zamyslieť, ako bude táto postupnosť vyzerať ak $\alpha = p/q$ kde $p, q$ sú prirodzené čísla. Teda ak je $\alpha$ racionálne číslo.

Čo však, ak je $\alpha$ **iracionálne číslo?** Teda napr. $\sqrt{2}$, $\pi$ alebo zlatý rez. Uvažujme $\alpha = \frac {1 +\sqrt{5}}{2} - 1 \approx 0.61803...$ a pozrime sa, aká postupnosť svetiel nám vznikne:

| číslo kroku         | S     | prekročilo sa celé číslo? |
|--------------|-----------|------------|
| 1. | 0.6180...      | 🔴        |
| 2. | 1.2360...      | 🟢        |
| 3. | 1.8541...      | 🔴        |
| 4. | 2.4721...      | 🟢        |
| 5. | 3.0901...      | 🟢        |
| 6. | 3.7082...      | 🔴        |
| 7. | 4.3262...      | 🟢        |
| 8. | 4.9442...      | 🔴        |
| 9. | 5.5623...      | 🟢        |
| 10. | 6.1803...      | 🟢        |
| 11. | 6.7983...      | 🔴        |
| 12. | 7.4164...      | 🟢        |
| 13. | 8.0344...      | 🟢        |
| ... | ...      | ...        |

Pozrime sa na postupnosť svetiel, ale zapíšme to ako znaky 🔴 $\rightarrow 0$ a 🟢 $\rightarrow 1$. Teda máme

0101101011011...

Nech by ste sa snažili akokoľvek, **nenájdete v tejto postupnosti periódu.** Inak povedané, táto nekonečná postupnosť sa nedá zapísať ako opakovanie nejakej konečnej postupnosti. Teda je aperiodická.

Postupnosti svetiel 🔴/🟢, ktoré vzniknú vyššie uvedeným spôsobom pre nejaké iracionálne $\alpha$ sa volajú **"Sturmové slová"** (podľa matematika Sturma, ktorého meno je zapísané [na Eiffelovej veži](https://en.wikipedia.org/wiki/List_of_the_72_names_on_the_Eiffel_Tower)). Majú strašne veľa zaujímavých kombinatorických vlastností o ktorých si môžete prečítať na [wiki stránke](https://en.wikipedia.org/wiki/Sturmian_word).

A tu sa dostávame k mojej diplomovej práci. 

*Pre ľudí je z istých dôvodov zaujímavé hľadať v aperiodických postupnostiach akési čiastočné periódy.* Teda úseky, ktoré sa síce neopakujú nekonečne veľa krát, ale aspoň zopár krát. 
Napr. úsek "01011" sa vo vyššie uvedenej postupnosti 0101101011011...  zopakuje 2-krát, ale tretí krát už nie.
Tieto menšie opakovania nám umožňujú povedať niečo o číslach, ktorých desatiný rozvoj je identický danej postupnosti. Ak si zoberieme vyššie uvedenú postupnosť 0101101011011... a na začiatok pridáme "0.", tak dostaneme číslo "0.0101101011011...". To, čo vieme o týchto číslach povedať je, či sú *algebraické alebo transcendentálne* (viac o tom, čo to znamená, si viete prečítať v [tomto blogu](/2023/09/19/transcendental.html)). Hľadaním periodických úsekov v Sturmových slovách sa zaoberám v [3. kapitole, 1. sekcií mojej diplomky](/assets/files/diss.pdf). V následných sekciách sa zameriavam na tzv. Epistumové slová, čo sú podobné postupnosti, ale môžu obsahovať aj iné znaky ako iba "0" a "1".

Prečo sme však začali tento blog rozprávaním o priestupných rokoch?

Zopakujme si, že rok má presne 365.2421897... dní. Teda, ak má náš kalendár 365 dní, tak tých 0.2421897... dňa nám každý rok ujde. Teda po prvom roku nám ušlo 0.2421897... dňa; po druhom $2 \cdot 0.2421897... = 0.484376...$ dňa; po treťom $3 \cdot 0.2421897... = 0.726564...$ dňa.... Pripomína vám to niečo?


| rok         | koľko dní sme stratili     | treba priestupný rok? |
|--------------|-----------|------------|
| 1. | 0.2421897...      | 🔴        |
| 2. | 0.4843794...      | 🔴        |
| 3. | 0.7265691...      | 🔴        |
| 4. | 0.9687588...      | 🔴        |
| 5. | 1.2109485...      | 🟢        |
| 6. | 1.4531382...      | 🔴        |
| 7. | 1.6953279...      | 🔴        |
| 8. | 1.9375176...      | 🔴        |
| 9. | 2.1797073...      | 🟢        |
| 10. | 2.421897...      | 🔴        |
| 11. | 2.6640867...      | 🔴        |
| ... | ...      | ...        |

Takže sme došli k tomu, že **postupnosť toho, kedy treba dať priestupný rok, je vlastne Sturmové slovo.** Vyzerá to presne tak, ako by sme čakali: opakuje sa 🔴🔴🔴🟢 a po istom čase sa niečo trošku zmení. Vypíšme si to presne:



<p class="image-in-blog">
<img style="max-width:900px;" src="/assets/images/blog/leap_year/leap_year_structure.png" alt="Kedy by mali byť priestupné roky?"/>
<em>Zakrúžkované sú obdobia kedy sú štyri nepriestupné roky za sebou.</em>
</p>

Čo nám táto postupnosť hovorí? Že ak chceme mať čo najmenšie odchýlky v kalendári, tak priestupný rok máme mať vtedy, kedy je zelené svetlo. Teda každé 4 roky je priestupný rok a približne raz za 33 rokov sa pridá jeden nepriestupný rok. To je iné oproti kalendáru, ktorý používame (gregoriánskemu). Ten každých 100 rokov pridá 3 nepriestupné roky, okrem každých 400 rokov, kedy to neurobí.

A tak sme uzavreli kruh. Aké z toho plynie poučenie? Že priestupné roky v gregoriánskom kalendári sú síce ľahko zapamätateľné a pekne štrukturované, ale keby sme chceli byť presní, tak **vyššie uvedený návrh by nás udržal najviac v synchronizácií s našou slnečnou sústavou.**
A zároveň to ukazuje, že za takou trivialitou ako je priestupný rok sa môže skrývať tak **fascinujúca matematika.**

*titulný obrázok: Leap year, 1908 Description: Cartoon on theme of women proposing in leap years. Caption: "Maidens are eagerly awaiting ..." at [wikipédia](https://en.wikipedia.org/wiki/Leap_year#/media/File:PostcardLeapYearMaidensAre1908.jpg)*



<!-- 
🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴
🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴🟢🔴🔴🔴 $\cdots$


0100010001000010001000100010001000100010001000010001000100010001000100010001000010001000100010001000100010001000010001000100010001000100010000100010001000100010001000100010000100010001000100010001000100010000100010001000100010001000100010000100010001000100010001000100001000100010001000100010001000100001000100010001000100010001000100001000100010001000100010001000100001000100010001000100010001000010001000100010001000100010001000010001000100010001000100010001000010001000100010001000100010001000010001000100010001000100010000100010001000100010001000100010000100010001000100010001000100010000100010001000100010001000100010000100010001000100010001000100001000100010001000100010001000100001000100010001000100010001000100001000 -->

<!-- 

Koľko dní má jeden rok? 

Easy, ňe?

Počkááááááj!

Ha há! Rok má 365 dní ale raz za štyri roky má 366 dní.

Prečo to tak je?

🤷‍♀️

Odpoveď je v prvom rade, že to tak nie je. Najbližší priestupný rok je v roku 2024. Ak by logika hore fungovala, tak ďalší priestupný rok bude v roku 2028, potom 2032, 2036, ..., 2080, ..., 2096, 2100, ... Lenže, či už vás to prekvapuje alebo nie, rok 2100 nie je priestupný rok. Má iba 365 dní rovnako ako roky 2099 a 2001. Možno si teraz niekto v zadnej lavici spomenie, že každých 100 rokov priestupný rok zruší. Teda, že podľa pravidla: "každé štyri roky" by rok 2100, 2200, 2300, ..., mali byť priestupné, ale nie sú. Prečo to tak je?

🤷‍♀️

Lenže, ani tak to nie je. Kto si pamätá na obdobie kedy som sa narodil si určite spomenie, že jubilejný rok 2000 predsa len priestupný bol. Teda podľa predošlých dvoch pravidiel by mali byť priestupné roky 1996 a 2004, ale nie rok 2000. Tentoraz musí prísť pán historik z vedľajšej triedy, aby nám objasnil, že okrem predošlých dvoch pravidiel existuje aj tretie: každých 400 rokov predsa len priestuný rok je. 

Zhrňme si to. Zober si aktuálny rok (napr. 2023).

- Ak je deliteľný číslom 4, tak je priestupný.

- Avšak, ak je deliteľný číslom 100, tak predošlé pravidlo sa ruší a daný rok priestupný nie je.

- Avšak, ak je deliteľný číslom 400, tak predošlé pravidlo sa ruší a daný rok priestupný je.

Čím to je?

🤷‍♀️

Odpoveď nie je až taká zložitá ako predošlé pravidlá. Totiž jeden rok trvá približne 365.




Sokrates: Povedzže Galukón, uvažoval si niekedy nad tým koľko dní má jeden rok?

Glaukón: Akoby som neuvažoval, vznešený Sokrates?! Veď rok a deň sú predsa dve najdôležitejšie časové jednotky. Nie je ich pomer azda o toľko dôležitejší?

S: Dobre uvažuješ. Tak mi teda povedz koľko dní má jeden rok.

G: Tristo šeťdesiat päť, predsa.

S: Si si tým istý? Nezabudol si azda na niečo špeciálne čo sa deje raz za čas.

G: Pravdu máš. Veď každý štvrtý rok sa pridáva do kalendára jeden deň. Teda každý štvrtý rok má presne 2366 dní ..

S: Nesklamala ma tvoja bystrosť, ale predsa ešte nemala celkom možnosť vyniknúť. Si si istý, že si 


S: 365?
G:
- ...

- Zle

- Druhý pokus.

- ...

- 356 a každý štvrtok rok 366?

- ...

- Tiež zle.

- Jak to??!

- Kedy je najbližší priestupný rok? 

- V roku 2024.

- To, znamená, že priestupný rok bude aj v 2028, 2032, 2036, ..., 2024 + 4 krát x, ..., 2096, 2100, ...?

- Asi by vtedy mali byť.

- Veruže to tak nie je. V roku 2100 priestupný rok nebude?

- Ajkaramba! (TODO prepíš na iné slovo)

-  -->
