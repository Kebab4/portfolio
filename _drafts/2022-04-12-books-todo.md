---
layout: post
title: Books Wanted
subtitle: 
banner:
---

Sú konkrétne zoznamy, z ktorých by som si chcel viac prečítať:

- Great Books

Podľa tém

- Teológia
    - Kresťanská mystika
    - Porovnanie islamu a kresťanstva
    - Analytický budhizmus (ono to je trošku oxymoron)
    - Analytická kresťanská teológia
    - Vývoj katolíckej doktríny (Newman, ...)
    - Joseph Ratzinger
    - Karl Rahner
    - ...Balthasar
    - Newman
    - George Pell, ...
    - Špidlík
    - O púštnych otcoch (Carlo Carretto)
    - O cirkevných otcoch
- Filozofia
    - Nejakých existencialistov
    - Filozofia vedy (Popper)
    - Etika cností
    - Leibniz, Spinoza, Descartes
    - Scholastika
    - Grécki filozofi
    - Wittgenstein (?)
    - C.S.Lewis
    - Simon Weil
    - Elisabeth Anscombe
    - Iris Murdoch
    - Mary Midgley
    - Pieper
    - Miracles (C.S.Lewis)
- Veda
    - História matematiky
- Politika
    - 
- Ekonomika
    - O kryptomenách
    - Prečo funguje ekonomika. Načo sú burzy. Ako fungujú akcie a hovadiny...
- História
    - História Katolíckej cirkvi
    - 
- Beletria
    - Ruské klasiky
    - Slovenská dedina
    - Salinger
    - J.R.R. Tolkien
    - Lewis Caroll
    - Absurdná dráma
    - George Orwell
    - Arthur Conan Doyle
    - Denník vidieckeho farára
    - Patagónia
- Poézia
    - Slovenská poézia druhej polovice 20. storočia
    - Katolícka moderna
    - Joezf Urban, Milan Rúfus
    - Romantici
    - Beat Generation
    - T.S. Eliot
    - Dante 
    - Goethe
- Iné
    - Gotická Architektúra...
    - Kniha o umení
    - Emočná inteligencia
    - Jung...


Recommendations:
- Povolaní milovať - Štefan Žarnay - katolícky slovenský kňaz v USA o rodine
- Eros a jeho poslanie - Štefan Žarnay - katolícky slovenský kňaz v USA o rodine
- Tréning väzňa - edícia o cvičení
- Stvorení pre hrdinstvo - o príbehu bežcov na malte a málo stravy
- Revoluce v opravdovém jídle - o carbo diéte
- Veľký sen - niečo z afriky
- Don Bosco - hrubá :D
- Láska bez hraníc
- Nevyžádané rady mládeži 
- Návrat do hôr (Janiga) - odporúča Sima M.
- Požierači neba
- Kam sa podel môj syr
- Nenechte sebou manipulovat
- Na prahu promeny (Richard Rohr)

Čo som prečítal:
- 2023:
    - Etika cností: pozri kurz
- 2022:
    - Tá knižka čo som prečítal vo vlaku na Sampor
    - Citadela, Exupéry - Fajné, ale bez dynamiky
    - Idea univerzity, Newman - Fajné, ale špeicifické
    - Inklings - Bibliografické, ok
- 2021:
    - Silmarillion - Fantastické
    - Cesta na Západ - prečítal v júli 2021. Pútavé čítanie o pútnikovi, kňazovi, ktorý ide do Lisieux a nocuje na farách. 
- Tvárou k zemi - Redmann - Nič moc
- Buď kde si - púštny otcovia vtipne -  Velice fajn
- Veľký rozvod - metafora neba/pekla - Fajné
- Malý princ - ideály detstva - skvel
- Divoký v srdci - mužská osobnosť
- Volám ťa žiť - veľa o čítaní písma - ok
- Miracles (Lewis)not yet
- Suburbanizácia BA - asi už neakutálne
- Hlad po Bohu - úvod do kresťanstva a osobnej modlitby - celkom fajn
- Evangelium gaudium - František, dobré myšlienky
- Listy z púšte - Carretove myšlienky z púšte - skvelé
- Kauza Kristus - evidencia Krista - fajné
- Ján Mária Vianney - životopis, klasika svätý, aj s chybami - ok
- Arthur Conan Doyle
- Traja Pátrači
- Jules Verne
- Húrinove Deti