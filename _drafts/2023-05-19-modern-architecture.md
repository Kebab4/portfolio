---
layout: post
title: Veslovanie
subtitle: aneb spoločné trable Oxfordských študentov
banner:
---

Všetci poznáme tie krásne staré gotické, neogotické, klasické, či neoklasické budovy Oxfordu. Ale vedeli ste, že toto čarovné mesto v sebe skrýva aj množstvo nie celkom "starej" architektúry. Veru, vystriedalo sa tu už mnoho architektov. Niektorí pohoreli, iní zahviezdili. Nechám na vaše posúdenie.

Potreba stavať nové budovy bola v druhej polovici aspoň taká ako je teraz a aká bude neskôr -- veľká. Už teraz je Oxfordká univerzita na počet študentov relatívne malá v porovnaní s niektorými európskymi univerzitami. Bolo treba stavať nové ubytovacie priestory, fakulty, knižnice, a aj nové college.

## Brutalizmus

A prečo neobohatiť tradičný žltý kameň šedým betónom?