---
layout: post
title: Lingvistické jednuhubky
subtitle: 
banner:
---

Vedeli ste, že ...

...v jednom z mnohých indických jazykov (konkrétne ...) existuje iba jedno slovo pre raňajky, obed, večeru... jednoducho slovo "jedlo".

...v Yorkshire je kultúra small talku taká, že keď sa stretnete, tak pred tým, než sa začnete REÁLNE rozprávať, tak najprv si vymeníte kludne aj 8 viete typu: "Ako sa máš? Dobre. A ty? Dobre. Jak ide život. Ale dá sa. A tebe? Ale hej..."

...skúste správne vysloviť nasledujúcu vetu: "through tough thorough thought though". A potom si to dajte vysloviť translátorom.

...
