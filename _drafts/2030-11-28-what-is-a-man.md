---
layout: post
title: What is a man?
subtitle:
banner:
---

V posledných rokoch je rozbehnutá debata o definícii muža/ženy. Teda, kto je to muž/žena, koľko existuje rodov, atď. Keďže má táto debata rozsiahle dôsledky a mám dojem, že jej chýba analytické uchopenie, rozhodol som sa urobiť krátku laickú úvahu, ktorá by mohola priniesť trochu svetla (light, not heat) do tejto debaty. Keďže tento problém je v jadre lingvistický, budem sa mu tak aj venovať.

tldr:
- Podľa mňa by sme mali úradne a administratívne ľudí registovať podľa ich génov (teda na základe chromozómu).
- Do športov by sme ich mali rozdeľovať na základe toho, akú pubertu prekonali (prípadne pri malých/nekompetitívnych športoch môže byť dohoda, že ľudia sa príjmu na základe toho, čo si povedia).
- Spoločensky by sme mali ľudí rozdeľovať pohlavne (teda aké majú pohlavné ústrojenstvo) a to zisťovať na základe konvenčných znakov alebo biologických črtov.
- Pri osobnom styku by malo byť tolerované brať ľudí defaultne podľa konvencií a biologických črtov. Avšak v prípade keď niekto zažiada o zmenu jazyka v úprimnosti a z psychologických dôvodov by sa mu malo okolie prispôsobiť.




Pred tým, než sa pokúsim uchopiť jednotlivé definície muža/ženy si zadefinujem pojmy, ktoré mi budeú v ďalších úvahach nápomocné. Najdôležitejším je definícia rôznych typov mužov a žien. Týchto typov by mohlo byť aj viac, no pre účely našej úvahy stačia tieto.
- genetický-muž je človek, ktorý dostal pri počatí chromózony XY, genetická-žena je človek, ktorý dostal pri počatí chromozóny XX. Tento typ sa spája s tým, aké má kto pohlavné orgány (pozn., ak si dá niekto vyoperovať/naoperovať pohlavný orgán, zjavne to nemení jeho typ, toto je situácia 3. typu)
- pubertálny-muž je človek, ktorý si prešiel pubertov s prevažne "mužskými" hormónmi (testosterón), pubertálna-žena je človek, ktorý si prešiel prevažne "ženskou" pubertou. Tento typ sa spája s viac či menej svalnatejším telom, s krivkami tela, s ochlpením na brade, s hrubším hlasom a inými fyzickými parametrami.
- pohlavný-muž/pohlavná-žena je človek, ktorý má mužsky/žensky vyzerajúce pohlavné orgány. Teda, ak sa niekto narodil ako genetický-muž, ale preoperoval si orgány, takže má (hoci nefunkčnú) vagínu a nemá penis, tak je pohlavná-žena.
- konvečný-muž/konvenčná-žena je človek, ktorý sa oblieka a správa ako "muž"/"žena" podľa dobových kovencií. Tento typ môžeme nazvať konvenčný a spája sa so štýlom obliekania, s účesmi a dĺžkou vlasov, s farbami šiat, ... Nespája sa s profesiou, obľúbenými aktivitami, či charakterom.
- pocitový-muž/pocitová-žena je človek, ktorý sa cíti ako muž/žena predošlých typov. Teda, cíti sa ako keby bol alebo mal byť narodený s chromzómami XY/XX, prešiel mužskou/ženskou pubertou, alebo mal byť konvečne akceptovný ako príslušný typ.
- deklaratívny-muž/deklaratívna-žena je človek, ktorý sa o sebe hovorí, že je muž/žena.

Teraz sa pozrime na jednotlivé pojmy z pohľadu nasledujúcich kritérií. Binárnosť/fluidnosť naznačuje, či je možné byť niečo ako medzi mužom a ženou v danom type, alebo sú to striktne oddelené kategórie. Empirickosť označuje kto vie za akých okolností zistiť, či je niekto muž alebo žena daného typu.

| Typ         | Binárnosť/fluidnosť     | Empirickosť |
|--------------|-----------|------------|
| Genetický | Binárny typ (špeciálne situácie rozoberám v poznámke)    | Jednoznačné, avšak zistiteľné až pri podrobnejšom anatomickom skúmaní osoby.        |
| Pubertálny      | Zväčša binárny, no existujú aj medzi stavy  | Zväčša jasne rozoznateľný, niekedy nejednoznačné    |
| Pohlavný | Prakticky binárny typ (špeciálne situácie sú umelé alebo extrémne výnimočné) | Jednoznačné, avšak zistiteľné až pri skúmaní intímnych partií danej osoby.
| Konvečný      | Fluidný  | Zväčša jasne rozoznateľný, niekedy nejednoznačné       |
| Pocitový      | Fluidný  | Zistiteľný až na základe vnútorného stavu človeka alebo psychologickej analýzy, vie byť nejednoznačné       |
| Deklaratívny      | Fluidný  | Zistiteľný iba deklarovaním daného človeka, niekedy nejednoznačné       |

Pred tým, než prejdeme k definíciam muža/ženy je potrebné si ujasniť niekoľko "dividing points" na ktorých sa jednotlivé definície rozídu. Ide o situácie v živote, kedy treba kategorizovať človeka na základe nejakého typu a jednotlivé definície vyberú rôzne typy pre rôzne situácie. Tieto situácie sú nasledovné:
- Administratíva - čo má daný človek zapísané v dokladoch, ako je zapísaný v zdravotnej karte...
- Šport - v niektorých športoch existuje delenie na mužov a ženy. Ktorý typ sa tu má aplikovať?
- Verejný život - Na základe čoho by sa mali rozdeľovať ľudia na toaletách/školách/spoločenských tancoch/otázky vo formulároch...
- Bežná reč - náš jazyk používa 3 rody a preto je treba si vybrať ktoré rody použijeme na oslovovanie/pomenúvanie svojich známych alebo ľudí, ktorých stretneme

Nasledujúce definície muža/ženy považujú za správne, aby sa pri jednotlivých situáciach aplikovala daný typ. Definície ponúkam štyri na základe odhadovane najčastejších definícií v tejto téme:


| Definícia         | Administratíva     | Šport | Verejný život | Bežná reč  |
|--------------|-----------|------------|---------|------------|
|  Konzervatívna radikálna   |   Genetický   |   Genetický   |   Genetický | Genetický (ak nie je zistiteľné, tak Pubertálny + Konvenčný) |
|  Konzervatívna umiernená   |    Genetický   |   Pubertálny    |  Pohlavný | Deklaratívny (ak nebolo deklarované, tak Pubertálny + Konvenčný) |
|  Progresívna umiernená   |   Pohlavný (niekedy Pubertálny)   |   Pubertálny (pri profesionálnom športe, inak Deklaratívny)  |  Pohlavný/Deklaratívny (závisí od situácie) | Deklaratívny (ak nebolo deklarované, tak stredný/neurčitý rod) |
|  Progresívna radikálna   |   Deklaratívny   |   Deklaratívny   |   Deklaratívny | Deklaratívny (ak nebolo deklarované, tak stredný/neurčitý rod) |


Teraz sa môžeme pustiť do hodnotenia výhod/nevýhod jednotlivých definícií a ich problémov. Pôjdem postupne podľa situácií.

Pre ďalšie úvahy budem predpokladať, že ak je niekto deklaratívne muž (alebo žena), tak je aj pocitovo  muž (respektíve žena). Situácie kedy je niekto pocitovo inak ako deklarovane sú, prakticky, pre všetkých irelevantné.

Pri Administratínych situáciach:
- Genetický alebo Pohlavný typ uľahčuje administratívu, lebo je, prakticky, binárny a je ťažko meniteľný.
- Nie Genetický/Pubertálny typ spôsobuje chaos pri štatistikách, lebo zaradzuje ľudí s iným biologickým rozpoložením tela do rovnakej štatistickej kategórie (diagnósy chorôb, meranie korelácie puberty na iné časti života atď)
- V spoločnosti, kde je silná korelácia medzi Genetickými/Pohlavnými typmi a Konvenčnými/Pubertálnymi typmi, ľudia, ktorým nekorešponduje Deklaratívny typ s Konvečným/Pubertálnym a ani s Genetickým/Pohlavným, zažívajú nepríjmené situácie na úradoch/nemocniciach, keď si myslia, že maju zle napísaný pohlavie.
- TODO Feminimus - pubertálne a genetické ženy budú stále v nevýhode oproti deklaratívnym ženám (a zároveň genetickým mužom)
- Okašľávanie sociálnych dávok apod.

Pri Športe:
- Deklaratívny typ spôsobuje neprimeranú výhodu pre pubertálnych mužov oproti pubertálnym ženám a tým pádom deomtivuje pubertálne ženy od športu.
- Pri nie Deklaratívnom type je ľuďom, ktorým nekorešponduje Deklaratívny typ s Pubertálnym, neumožnené hrať v kategórií v ktorej by chceli a cítia sa konfortne.

Pri spoločenských činnostiach
- Toalety - deklaratívny spôsob môže viesť k nepríjmeným situáciam na toaletách. Rovnako pre nie pohlavný prístup.
- Škola - pri deklaratívnych prístupoch môže dôjsť k zmätku pri vývine dieťaťa ohľadom vlastnej sexuality
- Kultúra - stratia/umenšia sa sa rozdieli medzi Genetickými/Pubertálnymi mužmi a ženami, ktoré sú biologicko-sociálne

Pri jazyku
- Nie deklaratívny spôsob odhadu nepríjmenú situáciu prvého stretu
- Nie deklaratívny spôsob prístupu spôsobí dlhodobý spor v komunikácií o používanie zámen
- Prílišná opatrnosť spôsobí komplikovanosť - predstavovanie, písanie zámen apod.



Úvaha na záver:
Spoločnosť si vyvinula spoločenské normy na rozoznanie muža od ženy a keďže rozdiel medzi týmito pohlaviami preniká rôzne sféry života, dostal sa do jazky, či kultúry. Avšak, vždy boli a vždy budú ľudia, ktorí nepasovali do tabulieka a do kritérií a ktorí by sa lepšie cítili v iných rolách, ako im prikazovala spoločnosť. Vďaka prudkej liberalizácií v 20. storočí, kedy sa definitívne uvoľnil jednotlivec z pút svojej spoločnosti (či už to považujeme za dobré alebo zlé), bolo konečne možné ľudí v "šedých zónach" pomenovať a dať im priestor. Došlo k uvoľňovaniu a "fluidizácií" spoločenských konvencií. Zároveň ľudia, ktorí prežívali syndróm "cítenia sa ako v cudzom tele" (presnejšie v opačnom pohlaví) dostali priestor na vyjadrenie svojej situácie a dokonca aj medicínske prostriedky na zmenu pohlavia či opačný vývoj puberty. Avšak, stále boli vnímaní spoločnosťou ako podivínovia a iní a to ich traume nepomáhalo. Jediný uchopiteľný filozofický základ, ktorí by dokázal prijať tieto fenomény ako normálne avšak je, že sa predefinuje pojem muž a žena na deklaratívny typ. Teda človek nie je definovaný chromozónmi, či konvenciami, alebo svojim vnútorným postojom k týmto konvenciám alebo genetickým faktom. Nielenže tým nastalo veľké zmätenie v oblasti jazyka, pretoeže dlho používaní pojem prudko zmenil svoju definíciu, avšak čím ďalej sa táto definícia ustáľovalo, tým viac sa oddeľovala od konvencií až ostala vyprádznená. Definícia muža/ženy na základe deklarácie nás síce "oslobodzuje" od konvenčných noriem a jazyka, avšak vytvára problémy na mnohých ďalších odvetviach od administratívnych, kultúrnych, športových, lingvistických a v konečnom dôsledku rozbíja aj samotnú definíciu sexuality, či vytvára prekážky pre feinistické hnutia. Dostali sme sa teda do situácie, kedy je treba uznať, že jazyk nemožno ľubovoľne meniť a verejne pomenovateľné kategórie sú nutné pre fungovanie. Napríklad schopnosť lingivisticky oddeliť tých, ktorí si prešli mužskou/ženskou pubertou alebo tých, ktorí majú potenciu rodiť deti od tých, ktorí túto potenciu od počatia nemajú. Je teda nevyhnutné, že ak chce spoločnosť ostať pri používaní rodu v jazyku deklaratívnym spôsobom, bude buď lepšie, keď sa tento rod vymaže, alebo sa vytvoria nové lingivistické konštrukty na pomenovanie reality ako napríklad pohlavie, puberta atď. Otázka však je, či nebude v konečnom dôsledku jednoduchšie zjednotiť všetky tieto kategórie pod pojmami muž a žena s tým, že budeme spoločensky tlačiť na toleranciu a dbať o psychické zdravie jednotlivcov.
