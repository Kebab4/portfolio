---
layout: post
title: Fajné Múzeá
subtitle: 
banner:
---

last edit: 19.3.2023

Toto je zoznam múzeí/galérií/pamiatok kde som bol a kde stojí za to ísť

- Vatikánske múzeá -- Jeden deň nestačí. Sixtínska kaplnka, Aténska škola a iné fresky, trapérie, moderní autori...
- Pompeie -- Je to ako vrátiť sa skoro o 3 tisícročia dozadu a kráčať tými ulicami.
- Auschwitz/Birkenau -- Prísť tam a vidieť nenahradia žiadne dokumenty.
- Science Museum, Londýn -- Extrémne hravé.
- Natural History Museum, Londýn -- Extrémne dobre spracované.
- Escher Museum, Den Hague -- Vybuchne vám hlava. A krásny priestor.
- Historiska Museet, Stockholm -- Výborne upravené priestory a popisky ku jednotlivým periódam. Takto by mali vyzerať múzeá.
- Medeltidsmuseet, Stockholm -- Malé ale dobré. Model stredovekého Štockholmu v reálnej (aj miniatúrnej) veľkosti.
- Benaki Museum, Atény -- Dobrý výber obrazov. Súkromná galéria.
- Byzantine and Christian Museum, Atény -- Perfektne spracovaná história kresťanského umenia a kultúry (hlavne z Byzancie).
- Akropola a okolie, Atény -- Predstavovať si tých ľudí tam bolo asi najsilnejšie. (Grécke demokracie a filozofi by sa dali tak perfektne spracovať. Nepoznáte niekto dobré múzeum?)
