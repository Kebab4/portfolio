---
layout: post
title: Coming soon!
subtitle:
banner:
---

Na tejto stránke budú postupne pribúdať blogy napr. o:

### OXFORD OXFORD OXFORD
- [ ] Mesačný newletter
- [ ] College life
- [ ] Random prednáška vysvetlená
- [ ] Nečakané
- [ ] Daily life
- [ ] Tolkien a Rings of Power

### Teológia
- [ ] Prosebná modlitba
- [ ] Odovzdanosť
- [ ] Spása a jej kolektívny element
- [ ] Milosť posväcujúca - diskrétna či spojitá?
- [ ] Prvá mnohosť a Trojica
- [ ] Eucharistia a transubstanciácia
- [ ] Ja a Všeobecná cirkev
- [ ] All the problems of Catholic Church
- [ ] Kto už sa napil starého víno, nemá chuť na mladé.

### Filozofia 
- [ ] Snaha o stručnú a predsa podrobnú analýzu otázky life/choice?? Má to zmysel??
- [ ] Snaha o pochopenie dogiem dnešnej liberálnej ľavice
- [ ] Prečo som unavený z kultúrnych vojen
- [ ] Je sloboda náhoda?
- [ ] Námaha, obeta, odvaha
- [ ] Prvé axiomy/princípy
- [ ] Predpokladáš toho príliš veľa
- [ ] Nie celkom random otázky
- [ ] Anarchia, sloboda, absolútna zvrchovanosť, zodpovednosť, kolektív
- [ ] Zvieratá a moralisti 22. storočia
- [ ] Posledné kritérium hodnotenia filozofie: žiteľnosť alebo logickosť?


### Sentimentálna či osobná chvíľka (absolútne žasnem prečo je práve tento riadok nesprávne sformátovaný)
- [ ] Viedeň a leto
- [ ] Kolégium
- [ ] Hory a samota
- [ ] Mních a samota
- [ ] Hľadanie pravdy a skutočný život
- [ ] Random otázky
- [ ] Túžba všetko celostne a dokonale vyjadriť
- [ ] Kľúče k dokonalosti (poznanie neresti, ...)
- [ ] Istota a moje rozhodnutia (implicit DS)
- [ ] Čo je pravda? Môžem veriť svojim slovám?
- [ ] Pali Kebis a správne veci - moja analýza osobnosti (spasiteľský syndróm)
- [ ] Očakávania život s nimi / bez nich. Valuácia očakávaní
- [ ] Strach z chýb

### Lingvistika
- [ ] Prečo je metafyzika vlastne ligvistika
- [ ] Inverzia privlastňovacieho prídavného mena v genitíve
- [ ] Prečo zbožňujem 2. pád (podstatných mien).

### Matematika a informatika
- [ ] O jazyku informatikov a zdeformovanej realite
- [ ] Prajnosť a závisť vo vedách a iných ľudských spoločenstvách
- [ ] O absolútnosti pravdy matematikovej
- [ ] Is god a mathematician?
- [ ] Zopár perkných obrázkov pre laické publikum
- [ ] Dôležitosť elegancie v živote matematika
- [ ] Typy matematikov
